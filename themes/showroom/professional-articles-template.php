<?php 
/* 
Template Name: Professional Articles Template
*/

remove_action( 'genesis_after_endwhile', 'genesis_posts_nav' );
remove_action(	'genesis_loop', 'genesis_do_loop' );
add_action(	'genesis_loop', 'blog_filter_template' );
add_action(	'genesis_before_content', 'blog_custom_sidebar');
function blog_filter_template() {
wp_reset_query();
	//global $post;
	//global $query_args;
	
    $args = array(
        'post_type' => 'post',
	'posts_per_page' => -1,
	'cat' => '13'
    );

// The Query
query_posts( $args );

// The Loop
while ( have_posts() ) : the_post();
    echo '<li>';
    ?><a href="<?php the_permalink(); ?>"><?php the_title('<h2>','</h2>'); ?></a><?php
    the_excerpt();
    echo '</li>';
endwhile;


// Reset Query
wp_reset_query();
} 


function blog_custom_sidebar() {
	get_sidebar();
}

genesis(); 
?>