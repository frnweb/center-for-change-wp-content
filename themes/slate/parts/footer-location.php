<section id="location" role="contentinfo">
                    	<div class="inner section">
                    	<div class="row expanded" data-equalizer data-equalize-on="small">
                    		<div class="columns large-9 medium-6 large-push-3 medium-push-6">
                            <div class="map-container">
                            <?php
                            
                            $facilitylocation = get_field('facility_location', 'option'); 
                            $altphonecheck = get_field('altphone_check', 'option');
                            $altphonetext = get_field('altphone_text', 'option');
                            $altphonenumber = get_field('altphone_number', 'option');

                            ?>
                            <?php if( !empty($facilitylocation) ): ?>
                              <div class="acf-map" data-equalizer-watch>
                                <div class="contact__marker" data-lat="<?php echo $facilitylocation['lat']; ?>" data-lng="<?php echo $facilitylocation['lng']; ?>">
                                  
                                  <?php

                                  $popupheader = get_field('popup_header', 'option');
                                  $popupselect = get_field('popup_select', 'option');
                                  $popupaddresstext = get_field('popup_address_text', 'option');
                                  $popupaddbutton = get_field('popup_add_button', 'option');
                                  $popupbuttontext = get_field('popup_button_text', 'option');
                                  $popupbuttontarget = get_field('popup_button_target', 'option');

                                    // Header
                                    if ($popupheader) {
                                      echo '<h2>'.$popupheader.'</h2>';
                                    }
                                    // Address
                                    if ($popupselect == "facility_address") { ?>
                                      
                                      <!-- Address -->
                                      <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                        <span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span> <br />
                                        <span itemprop="addressLocality"><?php the_field('city_state', 'option'); ?></span> 
                                        <span itemprop="postalCode"><?php the_field('zip_code', 'option'); ?></span>
                                      </p>
                                                
                                      <!-- Phone Number -->
                                      <p><span itemprop="telephone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Map Popover in Contact Module"]'); ?></span>
                                      </p>

                                    <?php
                                    }

                                    // Custom Address
                                    if ($popupselect == "custom_address") {
                                      echo $popupaddresstext;
                                    }
                                    
                                    // Button
                                    if ($popupaddbutton) {
                                      echo '<a class="button" href="'.$popupbuttontarget.'">'.$popupbuttontext.'</a>';
                                    } ?>

                                </div>
                              </div>
                            <?php endif; ?>
                            
                            </div><!-- end map-container -->
                           </div><!-- /.columns large-6 -->
                           
                    		<div class="columns large-3 medium-6 large-pull-9 medium-pull-6" itemscope itemtype="http://schema.org/Organization" data-equalizer-watch>
                            <h1 itemprop="name"><?php bloginfo('name'); ?></h1>
                            	<p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                                    <span itemprop="streetAddress"><?php the_field('street_address', 'option'); ?></span> <br />
                                    <span itemprop="addressLocality"><?php the_field('city_state', 'option'); ?></span> 
                                    <span itemprop="postalCode"><?php the_field('zip_code', 'option'); ?></span>
                             	</p>

                                <p>

                                  <span itemprop="telephone"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer Contact Info"]'); ?></span>

                                  <?php

                                  if ($altphonecheck) {
                                    echo 'or <span itemprop="telephone">';

                                      echo '<a href="tel:'.$altphonenumber.'" onClick="ga("send", "event", "phone", "alternate phone number");">'.$altphonetext.'</a>';

                                    echo '</span>';
                                  }
                                  ?>
                                </p>
                                
                                
                                <?php 
								$button = get_field('contact_page_button','option');
								$buttontarget = $button['button_target'];
								$buttontext = $button['button_text'];
								?>
                             
                               <a class="button button--footer" href="<?php echo $buttontarget ?>"><?php echo $buttontext ?></a>

                                <?php //The following helps with scanning databases that automatically update information about this facility. Numbers here are different as a result. ?>
                                <script type="application/ld+json">
                                { "@context" : "http://schema.org",
                                  "@type" : "MedicalOrganization",
                                  "url" : "<?php echo home_url('/'); ?>",
                                  "contactPoint" : [
                                    { "@type" : "ContactPoint",
                                      "telephone" : "<?php echo do_shortcode('[frn_phone only="yes"]'); ?>",
                                      "contactType" : "Admissions",
                                      "contactOption" : "Admissions",
                                      "areaServed" : "US",
                                      "availableLanguage" : ["English"]
                                    }] }
                                </script>
                            </div>
                    		<!-- /.columns large-6 -->
                    	</div><!-- /.row expanded -->
                        </div><!-- end inner -->
  </section><!-- end #location -->
  