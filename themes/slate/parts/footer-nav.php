<nav id="footerlinks" class="hidden">

	<div class="row expanded large-collapse medium-collapse small-collapse">
                	<div class="columns large-3 medium-3 small-4">
                    <a href="<?php echo home_url('/virtual-tour/'); ?>" target="_blank">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/tour-icon.svg" class="icon"/>
<span class="hide-for-small-only">&nbsp;Virtual Tour</span></a></div>
					
	<!-- /.col -->
	<div class="columns large-3 medium-3 hide-for-small-only" id="phoneinfooter">
    <span class="phone-number-footer">
    <i class="fa fa-mobile" aria-hidden="true"></i><span class="show-for-medium show-for-large" style="font-weight: 400;">&nbsp;Call Now</span> <span style="font-weight: 700;"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Floating Footer Bar"]'); ?></span></span></div>
    
	<!-- /.col -->
    
    
    <div class="columns large-3 medium-3 small-4">
     	<?php echo do_shortcode('[lhn_inpage button="email" id="footeremail" text=\'<i class="fa fa-envelope" aria-hidden="true" style="font-size: 24px; line-height: 120%;"></i><span class="hide-for-small-only">&nbsp;Email</span>\' ]'); ?>
     	<!--<a id="footeremail" onclick="window.open('http://www.livehelpnow.net/lhn/TicketsVisitor.aspx?lhnid=14160','Ticket','left=' + (screen.width - 550-32) / 2 + ',top=50,scrollbars=yes,menubar=no,height=550,width=450,resizable=yes,toolbar=no,location=no,status=no');return false;">
    		<i class="fa fa-envelope" aria-hidden="true" style="font-size: 24px; line-height: 120%;"></i><span class="hide-for-small-only">&nbsp;Email</span>
    	</a>-->
    </div><!-- END COL -->

    <div class="columns large-3 medium-3 small-4">
	<?php echo do_shortcode('[lhn_inpage button="chat" id="footerchat" text=\'<i class="fa fa-comments-o" aria-hidden="true"></i><span class="hide-for-small-only">&nbsp;Live Chat</span>\' offline=\'<i class="fa fa-comments-o" aria-hidden="true"></i><span class="hide-for-small-only">&nbsp;Chat Offline</span>\' ]'); ?>
	<!--<a id="footerchat" onclick="OpenLHNChat();return false; ga('send', 'event', 'Contact Options Flyout', 'Chat/Email');">
    		<i class="fa fa-comments-o" aria-hidden="true"></i><span class="hide-for-small-only">&nbsp;Live Chat</span>
	</a>-->
    </div> <!-- /.col -->
        </div><!-- END ROW -->
                </nav><!-- end footer links -->	
                
                
                