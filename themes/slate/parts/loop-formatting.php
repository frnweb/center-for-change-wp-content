<?php 
// This is where all of the sample formatting stuff goes
// Please note the code in the <textarea> requires no formatting/spacing tabs.
?>

<style>code{background: none; border: none;}textarea{overflow: hidden;}</style>
<br>

<div class="row">
	<div class="large-6 columns">
		<h1>h1. This is a very large header.</h1>
		<h2>h2. This is a large header.</h2>
		<h3>h3. This is a medium header.</h3>
		<h4>h4. This is a moderate header.</h4>
		<h5>h5. This is a small header.</h5>
		<h6>h6. This is a tiny header.</h6>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 200px;">
<h1>h1. This is a very large header.</h1>
<h2>h2. This is a large header.</h2>
<h3>h3. This is a medium header.</h3>
<h4>h4. This is a moderate header.</h4>
<h5>h5. This is a small header.</h5>
<h6>h6. This is a tiny header.</h6>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">

<div class="divider"></div>

	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 30px;">
[divider]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->

	<div class="medium-4 columns">
	<h2>Column Heading</h2>
	Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
	</div>	<!-- /.col columns -->
</div><!-- /.row -->


<div class="row">
	<div class="small-12 columns">
			<code class="clearfix">
			<textarea style="height: 500px;">
[row]
[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]
[/row]
		</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-12 columns">
		<div class="row small-up-2 large-up-4">
			<div class="column"><img src="https://placehold.it/300x300.jpg"></div>
			<div class="column"><img src="https://placehold.it/300x300.jpg"></div>
			<div class="column"><img src="https://placehold.it/300x300.jpg"></div>
			<div class="column"><img src="https://placehold.it/300x300.jpg"></div>
		</div><!-- /.col columns -->
	</div><!-- /.col columns -->
	<div class="large-12 columns">
		<code class="clearfix">
			<textarea style="height: 150px;">
[blockgrid screen="small" columns="2" altscreen="large" altcolumns="4"]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[/blockgrid]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.” <cite>Noah benShea</cite></blockquote>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 125px;">
<blockquote>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam eleifend turpis dui. Fusce id interdum dui, ac semper risus. Curabitur sed fermentum lorem. Vestibulum vitae efficitur augue, nectw.”
<cite>Noah benShea</cite></blockquote>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Un-ordered Lists</h4>
		<ul>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ul>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Un-ordered Lists</h4>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ul>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<br>
		<h4>Ordered Lists</h4>
		<ol>
		 	<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		 	<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		 	<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		</ol>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 275px;">
<h4>Ordered Lists</h4>
<ol>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
</ol>
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
<div class="callout">
<h2>This is a callout box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
<p><a href="#" class="button " target="_self">Find Out More</a>
</p></div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
		<code class="clearfix">
			<textarea style="height: 450px;">
[callout]
<h2>This is a callout box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
[button url="http://www.example.com"]Find Out More[/button]
[/callout]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
		<div class="callout equalizer" data-equalizer-watch>
		<h2>This is a callout equalizer box</h2>
		<ul>
		<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
		<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
		</ul>
		<p><a href="#" class="button " target="_self">Find Out More</a></p>
		</div>
	</div><!-- /.col columns -->
		<div class="large-6 columns">
		<div class="callout equalizer" data-equalizer-watch>
		<h2>This is a callout equalizer box</h2>
		<ul>
		<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
		<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
		<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
		<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
		<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
		</ul>
		<p><a href="#" class="button " target="_self">Find Out More</a></p>
		</div>
	</div><!-- /.col columns -->
	<div class="large-12 columns">
		<code class="clearfix">
			<textarea style="height: 620px;">
[callout_equalizer]
<h2>This is a callout equalizer box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
[button url="#"]Find Out More[/button]
[/callout_equalizer]

[callout_equalizer]
<h2>This is a callout equalizer box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
[button url="#"]Find Out More[/button]
[/callout_equalizer]
			</textarea>
		</code>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-5 columns">
<p><a href="#" class="button " target="_self">Button 1</a> <a href="#" class="button button--secondary" target="_self">Button 2</a></p>
	</div><!-- /.col columns -->
	<div class="large-7 columns">
			<textarea style="height: 75px;">
[button url="#"]Button 1[/button]
[button url="#" style="button--secondary"]Button 2[/button]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
<ul class="tabs " data-tabs="5yiap6-tabs" id="example-tabs1">
<li class="tabs-title is-active" role="presentation"><a href="#tabpanel1" role="tab" aria-controls="tabpanel1" aria-selected="true" id="tabpanel1-label">Tab One Title</a></li>
<li class="tabs-title " role="presentation"><a href="#tabpanel2" role="tab" aria-controls="tabpanel2" aria-selected="false" id="tabpanel2-label">Tab Two Title </a></li>
<li class="tabs-title " role="presentation"><a href="#tabpanel3" role="tab" aria-controls="tabpanel3" aria-selected="false" id="tabpanel3-label">Tab Three Title </a></li>
</ul>
<div class="tabs-content" data-tabs-content="example-tabs1">
<div class="tabs-panel is-active" id="tabpanel1" role="tabpanel" aria-hidden="false" aria-labelledby="tabpanel1-label">
<h2>Tab 1 Content</h2>
<p>This content is exclusive to tab one. Place your tab one content here.</p>
</div>
<div class="tabs-panel " id="tabpanel2" role="tabpanel" aria-hidden="true" aria-labelledby="tabpanel2-label">
<h2>Tab 2 Content</h2>
<p>This content is exclusive to tab two. Place your tab two content here.</p>
</div>
<div class="tabs-panel " id="tabpanel3" role="tabpanel" aria-hidden="true" aria-labelledby="tabpanel3-label">
<h2>Tab 3 Content</h2>
<p>This content is exclusive to tab three. Place your tab three content here.</p>
</div>
</div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
			<textarea style="height: 570px;">
[tabs]
[tab-title]Tab One Title[/tab-title]
[tab-title]Tab Two Title [/tab-title]
[tab-title]Tab Three Title [/tab-title]
[/tabs]

[tab-content]
[tab-panel]
<h2>Tab 1 Content</h2>
This content is exclusive to tab one. Place your tab one content here.

[/tab-panel]

[tab-panel]
<h2>Tab 2 Content</h2>
This content is exclusive to tab two. Place your tab two content here.

[/tab-panel]

[tab-panel]
<h2>Tab 3 Content</h2>
This content is exclusive to tab three. Place your tab three content here.

[/tab-panel]
[/tab-content]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">

	<div class="large-12 columns">
			<textarea style="height: 190px;">
[accordion]
[accordion-item title="Accordian Title 1"]
<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>

[/accordion-item]
[accordion-item title="Accordian Title 2"]This is my accordion content 2.[/accordion-item]
[accordion-item title="Accordian Title 3"]This is my accordion content 3.[/accordion-item]
[/accordion]
			</textarea>
	</div><!-- /.col columns -->
	
	<div class="large-12 columns">
<ul class="accordion" data-accordion="l4eitt-accordion" data-multi-expand="true" data-allow-all-closed="true" role="tablist">
<li class="accordion-item" data-accordion-item=""><a href="#" class="accordion-title" aria-controls="panel1" role="tab" id="panel1-label" aria-expanded="false" aria-selected="false">Accordian Title 1</a>
<div id="panel1" class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel1-label" aria-hidden="true"><!-- start accordion content --->
<h1>HTML Ipsum Presents</h1>

<p><strong>Pellentesque habitant morbi tristique</strong> senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. <em>Aenean ultricies mi vitae est.</em> Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, <code>commodo vitae</code>, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. <a href="#">Donec non enim</a> in turpis pulvinar facilisis. Ut felis.</p>

<h2>Header Level 2</h2>

<ol>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ol>

<blockquote><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus magna. Cras in mi at felis aliquet congue. Ut a est eget ligula molestie gravida. Curabitur massa. Donec eleifend, libero at sagittis mollis, tellus est malesuada tellus, at luctus turpis elit sit amet quam. Vivamus pretium ornare est.</p></blockquote>

<h3>Header Level 3</h3>

<ul>
   <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
   <li>Aliquam tincidunt mauris eu risus.</li>
</ul>
<!-- end accordion content --->
</div>
</li>
<li class="accordion-item" data-accordion-item=""><a href="#" class="accordion-title" aria-controls="panel2" role="tab" id="panel2-label" aria-expanded="false" aria-selected="false">Accordian Title 2</a>
<div id="panel2" class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel2-label" aria-hidden="true">This is my accordion content 2.</div>
</li>
<li class="accordion-item" data-accordion-item=""><a href="#" class="accordion-title" aria-controls="panel3" role="tab" id="panel3-label" aria-expanded="false" aria-selected="false">Accordian Title 3</a>
<div id="panel3" class="accordion-content" data-tab-content="" role="tabpanel" aria-labelledby="panel3-label" aria-hidden="true">This is my accordion content 3.</div>
</li>
</ul>
	</div><!-- /.col columns -->

</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-12 columns">
<div class="row expanded content-shortcode-row small-collapse large-uncollapse medium-uncollapse" data-equalizer="ecaqmh-equalizer" data-equalize-on="medium" id="test-eq" data-resize="nbpsmq-eq">
<div class="medium-12 large-4 columns">
<div class="callout equalizer contact-box" data-equalizer-watch="" style="height: 177px;">
<h1><i class="fa fa-mobile" aria-hidden="true"></i> Call Us</h1>
<p><span style="white-space:nowrap;">(602) 279-1468</span> (phone)<br>
<span style="white-space:nowrap;">(866) 767-6237</span> (toll-free)
</p></div>
</div>
<div class="medium-12 large-4 columns">
<div class="callout equalizer contact-box" data-equalizer-watch="" style="height: 177px;">
<h1><i class="fa fa-envelope-o" aria-hidden="true"></i> Email Us</h1>
<p><!-- LiveHelpNow On-Page Email Text Link --><a class="lhn_btn_email lhnEmailText ">Click here</a> to send us an email.
</p></div>
</div>
<div class="medium-12 large-4 columns">
<div class="callout equalizer contact-box" data-equalizer-watch="" style="height: 177px;">
<h1><i class="fa fa-comments-o" aria-hidden="true"></i> Chat Online</h1>
<p><!-- LiveHelpNow On-Page Chat Text Link --><a class="lhn_btn_chat">Click here to chat online.</a>
</p></div>
</div>
</div>
	</div><!-- /.col columns -->
	<div class="large-12 columns">
			<textarea style="height: 520px;">
[row]
[col large="4"]
[contact_box]
<h1><i class="fa fa-mobile"></i> Call Us</h1>
[frn_phone number="(602) 279-1468"] (phone)
[frn_phone] (toll-free)
[/contact_box]
[/col]

[col large="4"]
[contact_box]
<h1><i class="fa fa-envelope-o"></i> Email Us</h1>
[lhn_inpage button="email" text="Click here" ] to send us an email.
[/contact_box]
[/col]

[col large="4"]
[contact_box]
<h1><i class="fa fa-comments-o"></i> Chat Online</h1>
[lhn_inpage button="chat" text="Click here to chat online." offline="Online chats available only from 6am - midnight CST." ]
[/contact_box]
[/col]
[/row]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-6 columns">
<p><a class="button" data-open="foundationModal" aria-controls="foundationModal" id="sxzbc0-reveal" aria-haspopup="true" tabindex="0">Click Me to open Popover</a></p>
<div class="reveal-overlay" tabindex="-1" aria-hidden="true" style="display: none;"><div id="foundationModal" class="reveal" data-reveal="baer1a-reveal" aria-labelledby="sxzbc0-reveal" role="dialog" aria-hidden="false" data-yeti-box="foundationModal" data-resize="foundationModal" tabindex="-1" style="display: block; top: 210px;">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.<button class="close-button" data-close="" aria-label="Close modal" type="button"><span aria-hidden="true">×</span></button></div></div>
	</div><!-- /.col columns -->
	<div class="large-6 columns">
			<textarea style="height: 125px;">
[modal class="button" title="Click Me to open Popover"]Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.[/modal]
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-5 columns">

    <div class="orbit" role="region" aria-label="Website Name" data-orbit>
        <ul class="orbit-container">
            <button class="orbit-previous"><span class="show-for-sr">Previous Slide</span><i class="fa fa-caret-left" aria-hidden="true"></i></button><button class="orbit-next"><span class="show-for-sr">Next Slide</span><i class="fa fa-caret-right" aria-hidden="true"></i></button>
                <li class="is-active orbit-slide">
                    <img src="https://placehold.it/500x500.jpg" alt="" class="orbit-image"/>
                </li>
                <li class="is-active orbit-slide">
                    <img src="https://placehold.it/500x500.jpg" alt="" class="orbit-image"/>
                </li>
                 <li class="is-active orbit-slide">
                    <img src="https://placehold.it/500x500.jpg" alt="" class="orbit-image"/>
                </li>
        </ul><!-- end orbit container -->
    </div><!--- end orbit slider -->

	</div><!-- /.col columns -->
	<div class="large-7 columns" style="margin-top: 200px;">
			<textarea style="height: 60px;">
[facility_photo_slider]
(Upload images to Theme Settings > Facility Gallery)
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->

<div class="divider"></div>

<div class="row">
	<div class="large-12 columns" style="margin-top: 200px;">
			<h2 style="text-align: center;">Shortcode Kitchen Sink</h2>
			<textarea style="height: 2900px;">
[divider]

[row]
[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]

[col large="4"]
<h2>Column Heading</h2>
Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.
[/col]
[/row]

[blockgrid screen="small" columns="2" altscreen="large" altcolumns="4"]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[item]<img src="http://placehold.it/300x300.jpg" />[/item]
[/blockgrid]

[callout]
<h2>This is a callout box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
[button url="http://www.example.com"]Find Out More[/button]
[/callout]

[callout_equalizer]
<h2>This is a callout equalizer box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
[button url="#"]Find Out More[/button]
[/callout_equalizer]

[callout_equalizer]
<h2>This is a callout equalizer box</h2>
<ul>
<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Omnis, optio, voluptatum veniam beatae consequatur quam.</li>
<li>Deserunt, recusandae veritatis illum perspiciatis illo quae odio laboriosam laudantium consequuntur eveniet? Magni, tempore, animi.</li>
<li>Tempora, laboriosam, aspernatur, quisquam, recusandae pariatur hic dignissimos a inventore magni cupiditate quis eius. Nobis.</li>
<li>Ea nostrum libero beatae esse quisquam nihil accusantium eos adipisci! Assumenda cumque repellendus dolorem non.</li>
<li>Ipsum, possimus, modi dicta placeat impedit suscipit accusamus quis laborum voluptates ea odit fugit ab.</li>
</ul>
[button url="#"]Find Out More[/button]
[/callout_equalizer]

[button url="#"]Button 1[/button]
[button url="#" style="button--secondary"]Button 2[/button]

[tabs]
[tab-title]Tab One Title[/tab-title]
[tab-title]Tab Two Title [/tab-title]
[tab-title]Tab Three Title [/tab-title]
[/tabs]

[tab-content]
[tab-panel]
<h2>Tab 1 Content</h2>
This content is exclusive to tab one. Place your tab one content here.

[/tab-panel]

[tab-panel]
<h2>Tab 2 Content</h2>
This content is exclusive to tab two. Place your tab two content here.

[/tab-panel]

[tab-panel]
<h2>Tab 3 Content</h2>
This content is exclusive to tab three. Place your tab three content here.

[/tab-panel]
[/tab-content]

[accordion]
[accordion-item title="Accordian Title 1"]This is my accordion content.[/accordion-item]
[accordion-item title="Accordian Title 2"]This is my accordion content 2.[/accordion-item]
[accordion-item title="Accordian Title 3"]This is my accordion content 3.[/accordion-item]
[/accordion]

[row]
[col large="4"]
[contact_box]
<h1><i class="fa fa-mobile"></i> Call Us</h1>
[frn_phone number="(602) 279-1468"] (phone)
[frn_phone] (toll-free)
[/contact_box]
[/col]

[col large="4"]
[contact_box]
<h1><i class="fa fa-envelope-o"></i> Email Us</h1>
[lhn_inpage button="email" text="Click here" ] to send us an email.
[/contact_box]
[/col]

[col large="4"]
[contact_box]
<h1><i class="fa fa-comments-o"></i> Chat Online</h1>
[lhn_inpage button="chat" text="Click here to chat online." offline="Online chats available only from 6am - midnight CST." ]
[/contact_box]
[/col]
[/row]

[modal class="button" title="Click Me to open Popover"]Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.[/modal]

[facility_photo_slider]
(Upload images to Theme Settings > Facility Gallery)
			</textarea>
	</div><!-- /.col columns -->
</div><!-- /.row -->