 		
 		
	  <?php 

		 if (is_single()) {
			 //no page title because it is doing it in another part 
		 }

		else {
			 echo get_template_part('parts/loop', 'pagetitle'); //grabs page title
		 }

	  ?>
		    	
		    	
 	
	  	<?php //this determines the layout and div containers for the archive type pages
                
				if ( is_search() ) {
					echo '<div id="content" class="inner page--resources page--search">';
					//echo '<span class="success label">This is the Search Page</span>';
					echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
					echo '<main id="main" class="large-12 medium-12 columns first" role="main">';
					}

				 elseif (is_singular( 'staff' )) {
					echo '<div id="content" class="inner page--staff page--staff--single">';
					//echo '<span class="success label">This is the Single Staff Page</span>';
					echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
					echo '<main id="main" class="large-12 medium-12 columns first" role="main">';
				 }

				 elseif (is_single()) {
                	echo '<div id="content" class="inner page--resources">';
					//echo '<span class="success label">Is Single Page</span>';
					echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';

					// start sidebar and main layouts
					$sidebarlayout = get_field('resources_sidebar_layout', 'option');
					$largesidebarwidth = get_field('large_resources_sidebar_width', 'option');
					$mediumsidebarwidth = get_field('medium_resources_sidebar_width', 'option');

					$largewidth = (12 - $largesidebarwidth);
					$mediumwidth = (12 - $mediumsidebarwidth);

					// sidebar
					if($sidebarlayout == 'left') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' columns" role="complementary">';
					}

					if($sidebarlayout == 'right') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' large-push-'.$largewidth.' medium-push-'.$mediumwidth.' columns" role="complementary">';
					}
					echo get_sidebar();
					echo '</div>';//end the sidebar

					// #main
					if($sidebarlayout == 'left') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' columns has-sidebar" role="main">';
					}
					if($sidebarlayout == 'right') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' large-pull-'.$largesidebarwidth.' medium-pull-'.$mediumsidebarwidth.' columns has-sidebar" role="main">';
					}

				}
				
				 else {
                	echo '<div id="content" class="inner page--resources">';
					//echo '<span class="success label">Nothing Defined</span>';
					echo '<div id="inner-content" class="row expanded large-collapse medium-collapse">';
					
					// start sidebar and main layouts
					$sidebarlayout = get_field('resources_sidebar_layout', 'option');
					$largesidebarwidth = get_field('large_resources_sidebar_width', 'option');
					$mediumsidebarwidth = get_field('medium_resources_sidebar_width', 'option');

					$largewidth = (12 - $largesidebarwidth);
					$mediumwidth = (12 - $mediumsidebarwidth);

					// sidebar
					if($sidebarlayout == 'left') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' columns" role="complementary">';
					}

					if($sidebarlayout == 'right') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' large-push-'.$largewidth.' medium-push-'.$mediumwidth.' columns" role="complementary">';
					}
					echo get_sidebar();
					echo '</div>';//end the sidebar

					// #main
					if($sidebarlayout == 'left') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' columns has-sidebar" role="main">';
					}
					if($sidebarlayout == 'right') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' large-pull-'.$largesidebarwidth.' medium-pull-'.$mediumsidebarwidth.' columns has-sidebar" role="main">';
					}
				}
         ?>
	
		
		   <?php if (have_posts()) : while (have_posts()) : the_post(); ?><!-- this is where we start to grab the content inside the loop -->

   		  <?php 
				if ( is_single() || is_singular('staff') ) { 
					echo '<div class="row expanded large-collapse medium-collapse"><div class="post_holder clearfix">';
					echo '<div id="post-'.get_the_ID().'" role="article">';
					
				}

				
				
				else {//stopped here not working just yet
					echo '<div class="row expanded"><div class="post_holder clearfix">';
	 	 			echo '<div id="post-'.get_the_ID().'"';
					echo post_class( 0 === ++$GLOBALS['wpdb']->wpse_post_counter % 2 ? 'even' : 'odd' );
					echo 'role="article">';	//this counts the posts in the archive loop and it adds the class of even or odd 
				}
				
				?>
	  				

        
						<?php
                // This conditional statement is for grabbing the featured image and lining up the columns 
                
				if ( is_search() ) {
					echo '<div class="columns post_excerpt large-12 medium-12">';
					}

	  			elseif (is_singular('staff') ) {///this does something different with the Single posts
					
					$staffphoto = get_field('staff_photo');
					$firstname = get_field('first_name');
					$lastname = get_field('last_name');
					$title = get_field('title');
					$certifications = get_field('certifications');
					
					echo '<div class="columns post_excerpt large-12 medium-12">';
					echo '<article id="post-'.get_the_ID().'"';
					echo post_class('');
					echo 'role="article" itemscope itemtype="http://schema.org/BlogPosting">';
					echo '<section class="entry-content staff__member" itemprop="articleBody">';
					echo '<div class="row expanded collapse">';
					
					if( !empty($staffphoto) ){
					 echo '<div class="columns large-4 medium-6 small-12">';
					 echo '<div class="staff__member__info">';
					 echo '<div class="staff__member__info__photo">';	
					 echo '<img src="'.$staffphoto['url'].'" alt="'.$staffphoto['alt'].'" />';
					 echo '</div>';//end staff__member__info__photo	
					 echo '<h2>';
						   if ($firstname) {
							echo $firstname;
							}
							if ($lastname) {
							echo '&nbsp;'.$lastname.'';
							}
							if ($certifications) {
							echo '<span class="certifications">&nbsp;'.$certifications.'</span>';
							}
					 echo '</h2>';
						 if ($title) { 
							 	echo '<h3>'.$title.'</h3>';
						 }
					 
					 echo '</div>';//end staff__member__info
					 echo '</div>'; //end the first column with the image 
					 echo '<div class="columns large-8 medium-6 small-12">';
					 echo the_content();
					 echo '</div>'; //end the first column with the bio

					}
					
					else {//conditional statement for if they don't have a bio photo 
						echo '<div class="columns large-12">';
						echo '<h2>';
						   if ($firstname) {
							echo $firstname;
							}
							if ($lastname) {
							echo '&nbsp;'.$lastname.'';
							}
							if ($certifications) {
							echo '<span class="certifications">&nbsp;'.$certifications.'</span>';
							}
					 echo '</h2>';
						 if ($title) { 
							 	echo '<h3>'.$title.'</h3>';
						 }
					 echo the_content();
					 echo '</div>'; //end the first column with the bio
					}///end the else conditional statement
					
					echo '</div>'; //end row 
					echo '</section>';//end section
					echo '</article>';//end article
					

				}

				elseif (is_single() ) {///this does something different with the Single posts
					echo '<div class="columns post_excerpt large-12 medium-12">';
					echo '<article id="post-'.get_the_ID().'"';
					echo post_class('');
					echo 'role="article" itemscope itemtype="http://schema.org/BlogPosting">';
					echo '<section class="entry-content" itemprop="articleBody">';
					echo '<p class="sl_disclaimer">Please note that this is an Archived article and may contain content that is out of date.  The use of she/her/hers pronouns in some articles is not intended to be exclusionary. Eating disorders can affect people of all genders, ages, races, religions, ethnicities, sexual orientations, body shapes, and weights.</p>';
					echo the_content();
        			echo '<a class="button share"><span class="st_sharethis_custom">Share</span></a>';
					echo '</section>';//end section
						
					echo '<footer class="article-footer">';
					echo wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); 
					echo '</footer>';//end footer
					echo '</article>';//end article

				}
				
				  elseif ( is_archive() && has_post_thumbnail() || has_post_thumbnail() ) {
					//If it is not the single post page then we are taking the featured image and alternating them  
					echo '<div class="columns featured_image large-5 medium-12">';
                    echo '<a href="'.get_the_permalink().'">';
					echo the_post_thumbnail('full');
					echo '</a>';
					echo '</div>';
					echo '<div class="post_excerpt columns large-7 medium-12">';
                }
				
				 else {
                	echo '<div class="columns post_excerpt large-12 medium-12">';
				}
                ?>
       
        		
    		
    		   <?php 
				if ( is_single() ) { 
					///does nothing
				}
				
				else {
				echo '<header class="post-header">';
				echo '<h2><a href="'.get_the_permalink().'" rel="bookmark">';//need to figure out how to add the title attribute
				echo the_title();
				echo '</a></h2>';
				echo '</header>';
    			echo '<section class="post-excerpt" itemprop="articleBody">';
				echo the_excerpt('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); 
				echo '</section>';
				}
				
				?>
        
        		
            
        </div><!-- /.columns large-7 medium-5 -->
        
          
 </div> <!-- end #post-->
       </div><!-- end post_holder -->
    </div><!-- /.row expanded --><!-- all of these divs are being called on in the conditional statements above and should not be deleted -->
		
		<?php endwhile; ?>	
	
		
		<?php joints_page_navi(); ?>
					
		<?php else : ?>
											
		<?php get_template_part( 'parts/content', 'missing' ); ?>
						
		<?php endif; ?>
					
			</main><!-- end main -->
			
	
	    
</div><!-- end #inner content -->
</div><!-- end #content -->

    
   
						
				    						
