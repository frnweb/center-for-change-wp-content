

<div class="title-bar show-for-small-only" data-responsive-toggle="top-bar-menu">
 	<a href="<?php echo home_url('/'); ?>" class="show-for-small-only">									
		<!-- this grabs the Logo from the themes page, if they do not upload one it defaults to the one below -->
		<?php if( get_field('site_logo', 'option') ): ?>
		  <img src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />
			
		<?php else :?>
		  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/generic_logo.svg" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />
		<?php endif; ?>
	</a>

  <!--start the mobile menu button -->
	<div id="mobilemenu" class="show-for-small-only small-3 columns">
		<ul class="menu">
			<li>
				<button type="button" class="hamburger nav-button hamburger--squeeze" data-toggle="top-bar-menu" onClick="ga('send', 'event', 'hamburger Menu', 'opens mobile menu button');">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
			</li>
			<li><a data-toggle="top-bar-menu"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
		</ul><!-- end ul.menu -->
	</div><!-- end #mobilemenu -->

</div>


<div class="top-bar has-title-bar" id="top-bar-menu">
	
	<!-- LEFT SECTION -->
	<div class="top-bar-left show-for-medium">
		<a href="<?php echo home_url('/'); ?>" class="show-for-medium">									
		<!-- this grabs the Logo from the themes page, if they do not upload one it defaults to the one below -->
		<?php if( get_field('site_logo', 'option') ): ?>
		  <img src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />
			
		<?php else :?>
		  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/generic_logo.svg" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />	

		<?php endif; ?>
		
		</a>
	</div>
	
	<!-- RIGHT SECTION -->
	<div class="top-bar-right">

		<div class="mobile-menu show-for-small-only">
			<?php joints_top_nav(); ?>
		</div>
		

		<div class="row expanded large-collapse medium-collapse show-for-medium">
			<div class="large-7 medium-7 columns">
				<?php joints_top_nav(); ?>
			</div>

			<div id="usernav" class="large-3 medium-3 columns">
				<nav id="user-based">	
					<?php wp_nav_menu( array( 'theme_location' => 'user-links', 'depth' => 1) ); ?>
				</nav><!-- end user-based -->
			</div><!-- /.large-7 -->
																
			<div id="callinfo" class="large-2 medium-2 columns">
				<div id="phone">
					<h3>
						Call <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Desktop Header"]'); ?>
					</h3>
				</div><!-- end #phone -->
			</div><!-- #call info -->
		</div><!-- /.row -->

	</div><!-- /RIGHT SECTION -->


</div>