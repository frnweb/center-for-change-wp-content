<?php

	$card = get_sub_field('wildcard');

	// Link Wrap Fields
    $addlink = $card['add_link'];
    $linktarget = $card['link_target'];

	if ($addlink) {
	  $cardtagopener = 'a';
	  $cardtagcloser = '</a>';
	  $cardlink = ' href="'.$linktarget.'"';
	  $cardlinkclass = ' wildcard--link';
	} else {
	  $cardtagopener = 'div';
	  $cardtagcloser = '</div>';
	}

	// Define Card Holder
	$cardholder = '<'.$cardtagopener.''.$cardlink.' class="wildcard__holder'.$cardlinkclass.'" data-equalizer-watch>';
	    // are there any rows in our Wildcard?
	    if( have_rows('wildcard_content') ) {


	      	// loop through all the rows of flexible content
	      	
	    	echo $cardholder;// <div or <a href=... class="wildcard"

		      	while ( have_rows('wildcard_content') ) { the_row() ;
					 	$wildcardheader = get_sub_field('wildcard_header');
						$wildcardsubhead = get_sub_field('wildcard_subhead');
						$wildcardparagraph = get_sub_field('wildcard_paragraph');
						$wildcardimage = get_sub_field('wildcard_image');
						$imageclass = get_sub_field('wildcard_image_class');
						$wildcardwysiwyg = get_sub_field('wildcard_wysiwyg');
						$buttontarget = get_sub_field('button_target');
		 				$buttontext = get_sub_field('button_text');

		 				if ($imageclass) {
		 					$imageclass = get_sub_field('wildcard_image_class');
		 				} else {
		 					$imageclass = 'wildcard__image';
		 				}

						// CARD HEADER
						if( get_row_layout() == 'wildcard_header_clone' ) {
							echo'<h2 class="wildcard__header">'.$wildcardheader.'</h2>';
						}

						// CARD SUBHEAD TEXT
						elseif( get_row_layout() == 'wildcard_subhead_clone' ) {
							echo'<h2 class="wildcard__subhead">'.$wildcardsubhead.'</h2>';
						}

						// CARD PARAGRAPH TEXT
						elseif( get_row_layout() == 'wildcard_paragraph_clone' ) {
							echo'<p class="wildcard__paragraph" data-equalizer-watch>'.$wildcardparagraph.'</p>';
						}

						// CARD IMAGE
						elseif( get_row_layout() == 'wildcard_image_clone' ) {
							
							echo '<div class="'.$imageclass.'">
									<img src="'.$wildcardimage['url'].'" class="img-responsive wildcard__image" />
								  </div>';
						}

						// WYSIWYG
						elseif( get_row_layout() == 'wildcard_wysiwyg_clone' ) {
							echo $wildcardwysiwyg;
						}

						// CARD BUTTON
						elseif( get_row_layout() == 'wildcard_button_clone' ) {
						  echo '<a href="'.$buttontarget.'" class="button button--wildcard">'.$buttontext.'</a>';
						}
		      		} // close the loop of flexible content
		      	echo $cardtagcloser;
		    } // close flexible content conditional 
	    ?>
	  <!-- End WILDCARD FLEXIBLE CONTENT FIELD -->


