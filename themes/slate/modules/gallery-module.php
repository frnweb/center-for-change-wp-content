<?php
/*
Module: Gallery
*/
?>

<?php 

$gallerywidth = get_sub_field('gallery_width');
$gallerycaptions = get_sub_field('gallery_captions');
$galleryimages = get_sub_field('photo_gallery');


?>

<section class="module gallery">
	<div class="inner expanded">

		<div class="row expanded">
			<?php
			echo '<div class="gallery__carousel columns large-'.$gallerywidth.' large-centered medium-'.$gallerywidth.' medium-centered">';
				
				// GALLERY
				if( $galleryimages ) { ?>
					<!-- Call bx-slider on #gallery -->
					<div class="slider__container">
						<div id="gallery">
					        <?php foreach( $galleryimages as $image ) { ?>

					            <li class="slide slide--gallery">
					            	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="img-responsive"/>
					                <?php
					                	// Caption
					                $gallerycaptions = get_sub_field('gallery_captions');
						                if ($gallerycaptions == 'true') {
						                	echo '<p>'.$image['caption'].'</p>';
						                }
					                ?>
					            </li>
					        <?php } ?>
						</div><!-- /#gallery -->
				
					</div><!-- /.slider__container -->
				<?php
				}// /GALLERY ?>

			</div><!-- /.gallery__carousel -->
		</div><!-- /.row -->
	</div><!-- /.inner -->
</section><!-- /.module.gallery -->