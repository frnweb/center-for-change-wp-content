<?php
/*
Module: Duo
*/
?>

<?php
	// header
	$optionheader = get_sub_field('optional_header');
	$optionheadertext = get_sub_field('optional_header_text');

	// module class/id
	$addclass = get_sub_field('add_moduleclass');
	$addid = get_sub_field('add_moduleid');
	$class = get_sub_field('module_class');
	$id = get_sub_field('module_id');

	if ($addclass) {
		$moduleclass = ' duo--'.$class.'';
	}
	if ($addid) {
		$moduleid = ' id="'.$id.'"';
	}

	// media
	$mediatype = get_sub_field('media_select');
	$mediaview = get_sub_field('media_view');
	$mediawidth = get_sub_field('media_width');
	$video = get_sub_field( 'duo_video' );
	$cardwidth = (12 - $mediawidth);

	// image
	$imagelinkselect = get_sub_field('image_linkselect');
	$imagelink = get_sub_field('image_link');

	// card
	$cardheader = get_sub_field('card_header');
	$cardsecondary = get_sub_field('card_secondary');
	$cardsecondarytext = get_sub_field('card_secondary_text');
	$addbutton = get_sub_field('add_button');
	$button = get_sub_field('button');
	$buttontarget = $button['button_target'];
	$buttontext = $button['button_text'];
	$duoimage = get_sub_field('image_url');

	// Gallery
	$duogallery = get_sub_field('duo_gallery');
	$duogallerycaptions = get_sub_field('duo_gallery_captions');
?>

<?php

	echo '<div'.$moduleid.' class="module duo duo--'.$mediatype.''.$moduleclass.'">';

?>

		<div class="inner expanded">

			<?php if ($optionheader) { ?>
				<div class="module__header">
					<h2><?php echo $optionheadertext ?></h2>
				</div>	<!-- end module header --> 	
			<?php } // /if $optionheader?>

		 	<div class="row expanded collapse">
			 	
			 	<?php
			 	// MEDIA Left or Right
		 		if($mediaview == "left") {
				echo '<div class="duo__media large-'.$mediawidth.' medium-'.$mediawidth.' columns" data-equalizer-watch="duo__equalizer">';
				}
				if($mediaview == "right") {
				echo '<div class="duo__media large-'.$mediawidth.' medium-'.$mediawidth.' large-push-'.$cardwidth.' medium-push-'.$cardwidth.' columns" data-equalizer-watch="duo__equalizer">';
				}// /MEDIA Left or Right


					// IMAGE
					if($mediatype == "image") { 
						if ($imagelinkselect != 'none') {
							echo '<a href="'.$imagelink.'" class="image__link">';
						}
					 		echo '<div class="duo__img" data-equalizer-watch="duo__equalizer" style="background-image: url('.$duoimage.');">';

					 		echo '</div>';

				 		if ($imagelinkselect != 'none') {
							echo '</a>';
						}
				 	}// /IMAGE 


				 	// GALLERY
				 	if($mediatype == "gallery") {
				 		
				 		if( $duogallery ) {
							
							echo '<div class="slider__container">';
								echo '<div id="duo__gallery">';
							        
							        foreach( $duogallery as $image ) {
							            
							            echo '<li class="slide slide--duo" data-equalizer-watch="duo__equalizer" style="background-image: url('.$image['url'].');">';

							                if ($duogallerycaptions) {
							                	echo '<p>'.$image['caption'].'</p>';
							                }
							            echo '</li>';// /.slide
									
									}
								echo '</div>';// /#duo__gallery
							echo '</div>';// /.slider__container

						}// /if $duogallery

				 	}// /GALLERY


				 	// VIDEO
				 	if($mediatype == "video") { ?>
				 		<div class="duo__videocontainer">
					 		<?php if( $video ) {
							  
							  echo '<div class="embed-responsive embed-responsive-16by9">';
	  						  	echo '<iframe class="embed-responsive-item" src="'.$video.'?rel=0&amp;showinfo=0"></iframe>';
							  echo '</div>';
							}// /if $video ?>
							
						</div>
				 	<?php
				 	}// /VIDEO ?>


				</div><!-- /.duo__media -->


				<?php
				// CONTENT depends on $mediaview
				if($mediaview == "left") {
			 	echo '<div class="duo__content duo__content--right large-'.$cardwidth.' medium-'.$cardwidth.' columns" data-equalizer-watch="duo__equalizer">';
				}
				if($mediaview == "right") {
				echo '<div class="duo__content duo__content--left large-'.$cardwidth.' medium-'.$cardwidth.' large-pull-'.$mediawidth.' medium-pull-'.$mediawidth.' columns" data-equalizer-watch="duo__equalizer">';
				} ?>

					<div class="card card--duo">
						<?php
						
						// Header
						if ($cardheader) {
											echo '<h2 class="card__header card__header--duo">'.$cardheader.'</h2>';//grabs the header IF they have one
						}
	                   
	                   	// Subhead/Paragraph
	                    echo '<'.$cardsecondary.' class="card__secondary card__secondary--duo">'
	                         .$cardsecondarytext.
	                        '</'.$cardsecondary.'>';
						
						// Button
						if ($addbutton) {
							echo '<a href="'.$buttontarget.'" class="button button--duo">'.$buttontext.'</a>';
						}// /Button ?>
					</div><!-- /.card.card-duo -->

				</div><!-- /CONTENT -->

			</div><!-- end .row -->
		</div><!-- end .inner -->
	</div><!-- end .duo -->

