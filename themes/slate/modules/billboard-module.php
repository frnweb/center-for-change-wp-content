<?php
/*
Module: Billboard
*/
?>

<?php
	$mediaselect = get_sub_field('media_select');
	$billboardview = get_sub_field('billboard_view');
	$contentwidth = get_sub_field('billboard_width');
	$mediawidth = ( 12 - $contentwidth );

	if ($mediaselect != 'none') {
		$billboardclass = ' billboard--'.$mediaselect.'';
	}

	// VIDEO
	$video = get_field('main_video', 'option');
	$addvideoimage = get_sub_field('add_video_image');
	$videoimage = get_sub_field('video_image');
	
	
 	// BACKGROUND
	
	$billboardbackground = get_sub_field('background_select');
	$billboardimage = get_sub_field('billboard_image');
	$backgroundimage = ' style="background-image: url('.$billboardimage.');"';

 	$header = get_sub_field('billboard_header');
 	$subhead = get_sub_field('billboard_subhead');

 	// BUTTON
 	$addbutton = get_sub_field('add_button');
 	$button = get_sub_field('button');
 	$buttontarget = $button['button_target'];
 	$buttontext = $button['button_text'];
 	$addbuttontwo = get_sub_field('second_button');
 	$buttontwo = get_sub_field('button_secondary');
 	$buttontwotarget = $buttontwo['button_target'];
 	$buttontwotext = $buttontwo['button_text'];
 	


echo '<div class="module billboard'.$billboardclass.'">';

	 		

	 		// VIDEO
	 		if ($mediaselect == "video") {

	 			echo '<div class="billboard__holder billboard__holder--video"'.$backgroundimage.'>';
	 				
	 			if ($billboardbackground == "video") {
		 			// BACKGROUND VIDEO FOR JONNY
		 				

		 					echo get_template_part('parts/video', 'background');//grabs the page title
		 					// ---------- ADD BACKGROUND VIDEO HERE ------------

		 				
		 			// /BACKGROUND VIDEO FOR JONNY
	 			}

	 				echo '<div class="inner expanded">';
	 					echo '<div class="row expanded" data-equalizer data-equalize-on="medium">';

				 			// VIDEO MODAL
					 		echo '<div id="home-video" class="reveal reveal--billboard" data-reveal data-close-on-click="true" data-animation-in="scale-in-down" data-animation-out="fade-out" data-reset-on-close="true">';
								
								if($video) { 
					            	  
					            	echo $video;//grab video link from theme settings 
					      		}	
						        	echo '<button class="close-button" data-close aria-label="Close modal" type="button">';
						            	echo '<span aria-hidden="true">&times;</span>';
						        	echo '</button>';
					        echo '</div>';
					        // /VIDEO MODAL


					        // VIDEO COLUMN
					 		if ($billboardview == 'left') {
						 		echo '<div class="billboard__video columns large-'.$mediawidth.' medium-'.$mediawidth.' large-push-'.$contentwidth.' medium-push-'.$contentwidth.' columns" data-equalizer-watch>';
						 	}
						 	if ($billboardview == 'center') {
						 		echo '<div class="billboard__video columns large-12 medium-12 large-centered medium-centered" data-equalizer-watch>';
						 	}
						 	if ($billboardview == 'right') {
						 		echo '<div class="billboard__video columns large-'.$mediawidth.' medium-'.$mediawidth.'" data-equalizer-watch>';
						 	}
						 		// VIDEO Image & Button
						 		if ($addvideoimage == 'true') {
						 			echo '<section id="video" role="banner" style="background: url('.$videoimage.') center center/cover no-repeat;"">';
						 		}
						 		else {
						 			echo '<section id="video" role="banner">';
						 		} ?>
						            	<div class="play-video">
					                		<a class="button__circle" data-open="home-video" onClick="ga('send', 'event', 'play button', 'opens the video');">
					                			<i class="fa fa-play" aria-hidden="true"></i>
					                		</a>
						                </div>

						        </section><!-- end #video  -->
							<?php
							echo '</div>'; // /VIDEO COLUMN
						    

						    // CARD COLUMN
						    if ($billboardview == 'left') {
						    	echo '<div class="card card--billboard card--billboard--left large-'.$contentwidth.' medium-'.$contentwidth.' large-pull-'.$mediawidth.' medium-pull-'.$mediawidth.' columns" data-equalizer-watch>';
						    }
						 	
						 	elseif ($billboardview == 'center') {
						 		echo '<div class="card card--billboard card--billboard--center columns large-12 large-centered medium-12 medium-centered" data-equalizer-watch>';
						 	}
						 
						 	elseif ($billboardview == 'right') {
						 		echo '<div class="card card--billboard card--billboard--right large-'.$contentwidth.' medium-'.$contentwidth.' columns" data-equalizer-watch>';
						 	}
								// Header
							 	if ($header) {
							 		echo '<h1 class="card__header card__header--billboard">'.$header.'</h1>';
							 	}
								
								// Subhead
								if ($subhead) {
									echo '<h2 class="card__subhead card__subhead--billboard">'.$subhead.'</h2>';
								}
						 		
								// Buttons
						 		if($addbutton) { ?>
								    <a href="<?php echo $buttontarget ?>" class="button"><?php echo $buttontext ?></a>
								<?php } ?>

								<?php if($addbuttontwo) { ?>
								    <a href="<?php echo $buttontwotarget ?>" class="button button--secondary"><?php echo $buttontwotext ?></a>
								<?php }

							echo '</div>';// /CARD COLUMN

						echo '</div>';// /.row

					echo '</div>';// /.inner

				echo '</div>';// /.billboard__holder

			} // /VIDEO




			// CAROUSEL
			if ($mediaselect == "carousel") {

					if( have_rows('billboard_carousel') ) {
						echo '<div class="slider__container">';
							echo '<div id="carousel--billboard"';

								while( have_rows('billboard_carousel') ) { the_row();
								
								$billboardimage = get_sub_field('billboard_image');
							 	$header = get_sub_field('billboard_header');
							 	$subhead = get_sub_field('billboard_subhead');
							 	
							 	// Buttons
							 	$addbutton = get_sub_field('add_button');
							 	$button = get_sub_field('button');
							 	$buttontarget = $button['button_target'];
							 	$buttontext = $button['button_text'];
							 	$addbuttontwo = get_sub_field('second_button');
							 	$buttontwo = get_sub_field('button_secondary');
							 	$buttontwotarget = $buttontwo['button_target'];
							 	$buttontwotext = $buttontwo['button_text'];

								?>
								
									<li class="slide slide--billboard">

										<?php
										echo '<div class="billboard__holder billboard__holder--carousel" style="background-image: url('.$billboardimage['url'].');">';
											echo '<div class="inner expanded">';
												echo '<div class="row expanded" data-equalizer data-equalize-on="medium">';

											 		if($billboardview == "left") {
												 		echo '<div class="card card--billboard card--billboard--left large-'.$contentwidth.' medium-'.$contentwidth.' small-12 columns">';
												 	}

												 	if($billboardview == "center") {
												 		echo '<div class="card card--billboard card--billboard--center large-12 large-centered medium-12 medium-centered small-12 columns">';
												 	}

												 	if($billboardview == "right") {
												 		echo '<div class="card card--billboard card--billboard--right large-'.$contentwidth.' medium-'.$contentwidth.' large-offset-'.$mediawidth.' medium-offset-'.$mediawidth.' small-12 columns">';
												 	}

													 	// Header
													 	if ($header) {
													 		echo '<h1 class="card__header card__header--billboard">'.$header.'</h1>';
													 	}
														
														// Subhead
														if ($subhead) {
															echo '<h3 class="card__subhead card__subhead--billboard">'.$subhead.'</h3>';
														}
												 		
														// Buttons
												 		if($addbutton) {
															echo '<a href="'.$buttontarget.'" class="button">'.$buttontext.'</a>';
														}

														if($addbuttontwo) {
															echo '<a href="'.$buttontwotarget.'" class="button button--secondary">'.$buttontwotext.'</a>';
														}

													echo '</div>';// /.card.card--billboard

												echo '</div>';// /.row

											echo '</div>';// /.inner

										echo '</div>';// /.billboard__holder
											

											?>
									</li><!-- /.slide -->

								<?php
								}// /while billboard_carousel

							echo '</div>';// /#carousel--billboard
						echo '</div>';// /.slider__container
					}// /have_rows billboard_carousel

			}// /CAROUSEL



			// NONE
			if ($mediaselect == "none") {

				echo '<div class="billboard__holder billboard__holder--basic"'.$backgroundimage.'>';
					
					if ($billboardbackground == "video") {
		 			// BACKGROUND VIDEO FOR JONNY
		 				echo get_template_part('parts/video', 'background');//grabs the page title
		 			// /BACKGROUND VIDEO FOR JONNY
	 				}
					
					echo '<div class="inner expanded">';
						echo '<div class="row expanded" data-equalizer data-equalize-on="medium">';

							// CARD COLUMN NO VIDEO
					 		if($billboardview == "left") {
						 	echo '<div class="card card--billboard card--billboard--left large-'.$contentwidth.' medium-'.$contentwidth.' small-12 columns" data-equalizer-watch>';
						 	}

						 	if($billboardview == "center") {
						 	echo '<div class="card card--billboard card--billboard--center large-12 large-centered medium-12 medium-centered small-12 columns" data-equalizer-watch>';
						 	}

						 	if($billboardview == "right") {
						 	echo '<div class="card card--billboard card--billboard--right large-'.$contentwidth.' medium-'.($contentwidth + 2).' large-offset-'.$mediawidth.' medium-offset-'.($mediawidth - 2).' small-12 columns" data-equalizer-watch>';
						 	}
							 	// Header
							 	if ($header) {
							 		echo '<h1 class="card__header card__header--billboard">'.$header.'</h1>';
							 	}
								
								// Subhead
								if ($subhead) {
									echo '<h3 class="card__subhead card__subhead--billboard">'.$subhead.'</h3>';
								}
						 		
								// Buttons
						 		if($addbutton) { ?>
								    <a href="<?php echo $buttontarget ?>" class="button"><?php echo $buttontext ?></a>
								<?php } ?>

								<?php if($addbutton == "true" && $addbuttontwo == "true") { ?>
								    <a href="<?php echo $buttontwotarget ?>" class="button button--secondary"><?php echo $buttontwotext ?></a>
								<?php }

							echo '</div>';// /CARD COLUMN NO VIDEO

						echo '</div>';// /.row

					echo '</div>';// /.inner

				echo '</div>';// /.billboard__holder
			}


			?>

</div><!-- end .module .billboard -->

