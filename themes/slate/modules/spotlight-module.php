<?php
/*
Module: Spotlight
*/
?>

<?php
	// module class/id
	$addclass = get_sub_field('add_moduleclass');
	$addid = get_sub_field('add_moduleid');
	$class = get_sub_field('module_class');
	$id = get_sub_field('module_id');
	
	if ($addclass) {
		$moduleclass = ' spotlight--'.$class.'';
	}
	if ($addid) {
		$moduleid = ' id="'.$id.'"';
	}

	// background image 
	$addbackground = get_sub_field('add_background');
	$backgroundimage = get_sub_field('background_image');

	if ($addbackground) {
		$background = ' style="background-image: url('.$backgroundimage['url'].');"';
		
		if ($addbackground && $addclass) {
			$moduleclass = ' spotlight--background spotlight--'.$class.'';
		} else {
			$moduleclass = ' spotlight--background';
		}
	}

 	$cardheader = get_sub_field('card_header');
	$cardsecondary = get_sub_field('card_secondary');
	$cardsubtext = get_sub_field('card_secondary_text');
	
	// buttons
	$addbutton = get_sub_field('add_button');
	$button = get_sub_field('button');
	$buttontarget = $button['button_target'];
	$buttontext = $button['button_text'];
	$addbuttontwo = get_sub_field('second_button');
	$buttontwo = get_sub_field('button_secondary');
	$buttontwotarget = $buttontwo['button_target'];
	$buttontwotext = $buttontwo['button_text'];

	$addphone = get_sub_field('add_phone_number');
?>

<?php

echo '<div'.$moduleid.' class="module spotlight'.$moduleclass.'"'.$background.'>';

?>

	<div class="inner expanded">
		<div class="card card--spotlight">
			
				<?php 

				// Header
				echo '<h2>'.$cardheader.'</h2>';
			    	
		    	// Phone Number
		    	if($addphone == "true") { 
					echo '<h1 class="phone phone--spotlight">'.do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Spotlight Module"]').'</h1>';
				}
				
				// Subhead/Paragraph
				if ($cardsubtext) {
					echo '<div class="card__secondary card__secondary--spotlight">';
				    	echo '<'.$cardsecondary.'>';
				    		echo $cardsubtext;
				    	echo '</'.$cardsecondary.'>';
			    	echo '</div>';
				}
				
				// Button
				if ($addbutton) {
					echo '<a href="'.$buttontarget.'" class="button button--spotlight">'.$buttontext.'</a>';
				}

				// Second Button
				if($addbuttontwo) { 
				    echo '<a href="'.$buttontwotarget.'" class="button button--secondary">'.$buttontwotext.'</a>';
				} ?>

		</div><!-- /.card-spotlight -->
	</div><!-- /.inner -->
</div><!--  /.module.spotlight -->




