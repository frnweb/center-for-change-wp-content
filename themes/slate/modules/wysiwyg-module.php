<?php
/*
Module: Content Editor
*/
?>

<?php
	// module class
	$addclass = get_sub_field('add_moduleclass');
	$class = get_sub_field('module_class');
	
	$wysiwygcontent = get_sub_field('wysiwyg_content');

// If Module Class
if ($addclass) {
echo '<div class="module wysiwyg wysiwyg--'.$class.'">';
} else {
echo '<div class="module wysiwyg">';
}// /If Module Class ?>

	<div class="inner">
		<div class="entry-content">
			
				<?php
					echo $wysiwygcontent;
				?>
		</div><!-- end entry content -->	
	</div><!-- end inner  -->
</div><!-- end module -->