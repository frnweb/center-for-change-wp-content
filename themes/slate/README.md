SLATE Version 0.0.1
Currently using Foundation 6.2.1 and Advanced Custom Fields Pro 5.5.3

### What is SLATE?

SlATE is a modular and scalable wordpress framework built on Foundations 6 and Advanced Custom Fields. SLATE runs on a Child/Parent concept meaning that to customize your site you MUST use a child theme. SLATE edits are made in one central repository, therefore changes should not be made within the core files. 

Download the child theme from the SLATE Child theme base to begin customizing some sites.

### What else do I need to know about SLATE? 
Slate is built from the Joints WP starter theme. JointsWP comes pre-baked with all of the great features that are found in the Foundation framework – simply put, if it works in Foundation, it will work in JointsWP. The theme also includes:

* Foundation Navigation Options
* Motion-UI
* Grid archive templates
* Translation Support
* Bower and Gulp Support
* And much, much more! 
