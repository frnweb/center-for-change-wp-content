<?php
/*
Shortcodes based on Foundations 6.0
 */

// adds row shortcode
function frn_foundation_row ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'style'		=> 'small-collapse large-uncollapse medium-uncollapse'
		), $atts );
	return '<div class="row expanded content-shortcode-row ' . esc_attr($specs['style'] ) . '" data-equalizer data-equalize-on="medium">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('row', 'frn_foundation_row' );

// adds column shortcode
function frn_foundation_column ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'screen'		=> 'medium',
		'columns'		=> '12',
		'altscreen'		=> '',
		'altcolumns'	=> '',
		), $atts );
	return '<div class="' . esc_attr($specs['screen'] ) . '-' . esc_attr($specs['columns']) . ' ' . esc_attr($specs['altscreen'] ) . '-' . esc_attr($specs['altcolumns']) . ' columns">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('col', 'frn_foundation_col' );

// adds column shortcode
function frn_foundation_col ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'small'		=> '12',
		'medium'	=> '12',
		'large'		=> '12',
		), $atts );
	return '<div class="small-' . esc_attr($specs['small']) . ' medium-' . esc_attr($specs['medium']) . ' large-' . esc_attr($specs['large']) . ' columns">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('col', 'frn_foundation_col' );

// adds block grid shortcode
function frn_foundation_blockgridul ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'screen'		=> 'medium',
		'columns'		=> '4',
		'altscreen'		=> '',
		'altcolumns'	=> '',
		), $atts );
	return '<div class="row ' . esc_attr($specs['screen'] ) . '-up-' . esc_attr($specs['columns']) . ' ' . esc_attr($specs['altscreen'] ) . '-up-' . esc_attr($specs['altcolumns']) .'">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('blockgrid', 'frn_foundation_blockgridul' );

// adds block grid list items
function frn_foundation_blockgridli ( $atts, $content = null ) {
	return '<div class="column">' .  do_shortcode ( $content ) . '</div>';
}

add_shortcode ('item', 'frn_foundation_blockgridli' );

// adds button control
function frn_foundation_buttons ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'url'	=> '#',
		'style'	=> '', 
		'target'	=> '_self'
		), $atts );
	return '<a href="' . esc_attr($specs['url'] ) . '" class="button ' . esc_attr($specs['style'] ) . '" target="'. esc_attr($specs['target'] ) .'">' . $content . '</a>';
	//return '<ul class="' . esc_attr($specs['screen'] ) . '-block-grid-' . esc_attr($specs['columns']) . '">' . $content . '</div>';
}

add_shortcode ('button', 'frn_foundation_buttons' );

// adds flex video shortcode
function frn_foundation_flexvideo ( $atts, $content = null ) {
	return '<div class="flex-video">' . $content . '</div>';
}

add_shortcode ('flexvideo', 'frn_foundation_flexvideo' );

// adds tabs, titles and tab content
function frn_foundation_tabs( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'style'		=> ''
	), $atts );
	static $i = 0;
	$i++;
	
	return '<ul class="tabs ' . esc_attr($specs['style'] ) . '" data-tabs id="example-tabs' . $i . '">' . do_shortcode ( $content ) . '</ul>';
}

add_shortcode ('tabs', 'frn_foundation_tabs' );

function frn_foundation_tabs_title ( $atts, $content = null ) {
	static $i = 0;
	if ( $i == 0 ) {
		$class = 'is-active';
	} else {
		$class = NULL;
	}
	$i++;
	$value = '<li class="tabs-title ' . $class . '"><a href="#tabpanel' . $i . '">' .
	$content . '</a></li>';

	return $value;
}

add_shortcode ('tab-title', 'frn_foundation_tabs_title' );

function frn_foundation_tabs_content( $atts, $content = null ) {
	static $i = 0;
	$i++;
	return '<div class="tabs-content" data-tabs-content="example-tabs' . $i . '">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('tab-content', 'frn_foundation_tabs_content' );

function frn_foundation_tabs_panel ($atts, $content = null ) {
	static $i = 0;
	if ( $i == 0 ) {
		$class = 'is-active';
	} else {
		$class = NULL;
	}
	$i++;
	return '<div class="tabs-panel ' . $class .'" id="tabpanel' . $i . '">' . $content . '</div>';
}

add_shortcode ('tab-panel', 'frn_foundation_tabs_panel' );

// accordions and associated
function frn_foundation_accordion( $atts, $content = null ) {
	return '<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">' . do_shortcode ( $content ) . '</ul>';
}

add_shortcode ('accordion', 'frn_foundation_accordion' );

function frn_foundation_tabs_accordion_item ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'title'		=> ''
		), $atts );
	static $i = 0;
/*	if ( $i == 0 ) {
		$class = 'active';
	} else {
		$class = NULL;
	}*/
	$i++;
	$value = '<li class="accordion-item" data-accordion-item><a href="#" class="accordion-title">' . esc_attr($specs['title'] ) . '</a><div id="panel' . $i . '" class="accordion-content" data-tab-content>' . $content . '</div></li>';

	return $value;
}

add_shortcode ('accordion-item', 'frn_foundation_tabs_accordion_item' );

function frn_foundation_modal ( $atts, $content = null ) {
	$specs = shortcode_atts( array(
		'title' => '',
		'class' => '',
		'id'	=> 'foundationModal',
		), $atts );
	return '<a class="' . esc_attr ($specs['class'] ) . '" data-open="' . esc_attr($specs['id']) . '">' . esc_attr ($specs['title']). '</a><div id="' . esc_attr ($specs['id']) . '" class="reveal" data-reveal>'
		. do_shortcode ($content) . '<button class="close-button" data-close aria-label="Close modal" type="button"><span aria-hidden="true">&times;</span></button></div>';
}

add_shortcode ( 'modal', 'frn_foundation_modal' );


///THESE ARE SOME SHORTCODES THAT ARE INDEPENDENT OF THE FOUNDATION FRAMEWORK



// Add Shortcode
function line_divider() {

	return '<div class="divider clearfix"></div>';

}
add_shortcode( 'divider', 'line_divider' );

// adds callout shortcode
function callout_box ( $atts, $content = null ) {
	return '<div class="callout">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('callout', 'callout_box' );


// adds large
function large_text ( $atts, $content = null ) {
	return '<div class="callout-text">' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('large_text', 'large_text' );

//callout with equalizer 
function callout_box_equalizer ( $atts, $content = null ) {
	return '<div class="callout equalizer" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('callout_equalizer', 'callout_box_equalizer' );

////map container class 
function map_container ( $atts, $content = null ) {
	return '<div class="map-container">' . $content . '</div>';
}

add_shortcode ('map', 'map_container' );

//contact box 
function contact_box_section ( $atts, $content = null ) {
	return '<div class="callout equalizer contact-box" data-equalizer-watch>' . do_shortcode ( $content ) . '</div>';
}

add_shortcode ('contact_box', 'contact_box_section' );

//facility slider
function facility_photo_slider($atts) {
  ob_start();
  get_template_part('parts/loop', 'gallery');
  return ob_get_clean();
}
add_shortcode('facility_photo_slider', 'facility_photo_slider');


//disables wp texturize on registered shortcodes
function frn_shortcode_exclude( $shortcodes ) {
    $shortcodes[] = 'row';
    $shortcodes[] = 'column';
    $shortcodes[] = 'blockgrid';
    $shortcodes[] = 'item';
    $shortcodes[] = 'button';
    $shortcodes[] = 'flexvideo';
    $shortcodes[] = 'tabs';
    $shortcodes[] = 'tab-title';
    $shortcodes[] = 'tab-content';
    $shortcodes[] = 'tab-panel';
    $shortcodes[] = 'accordion';
    $shortcodes[] = 'accordion-item';
    $shortcodes[] = 'modal';
	$shortcodes[] = 'divider';
	$shortcodes[] = 'map';
	$shortcodes[] = 'contact_box';
	$shortcodes[] = 'facility_photo_slider';
	$shortcodes[] = 'frn_privacy_url';
    $shortcodes[] = 'frn_footer';
    $shortcodes[] = 'lhn_inpage';
    $shortcodes[] = 'frn_phone';
    $shortcodes[] = 'frn_social';
    $shortcodes[] = 'frn_boxes';

    return $shortcodes;
}

add_filter ('no_texturize_shortcodes', 'frn_shortcode_exclude' );

// remove and re-prioritize wpautop to prevent auto formatting inside shortcodes
// shortcode_unautop is a core function

remove_filter( 'the_content', 'wpautop');
add_filter ( 'the_content', 'wpautop', 99 );
add_filter ('the_content', 'shortcode_unautop', 100 );

function my_acf_add_local_field_groups() {
    remove_filter('acf_the_content', 'wpautop' );
}
add_action('acf/init', 'my_acf_add_local_field_groups');