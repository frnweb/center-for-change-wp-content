<?php
// This file handles the admin area and functions - You can use this file to make changes to the dashboard.

function acf_admin_head_layout() {

    ?>

    <style type="text/css">

        /* Remove borders on li since we not removing li's  */
        .acf-fc-popup li {
            border:0 !important;
        }

        /* WordPress Template "home"
         * - hide ACF layouts named "block__quote", "block__text-img" and "block__list"
         */
        .has-template-home .acf-fc-popup a[data-layout="the_content"],
        .has-template-home .wp-editor-expand {     
            display: none;
        }

        /* WordPress Landing template
         */
        .has-template-landing .acf-fc-popup a[data-layout="billboard_clone"] {
            display: none;
        }

        /* WordPress default template
         * - hide ACF layout named "billboard_clone"
         */
        .has-template-default .acf-fc-popup a[data-layout="billboard_clone"],
        .has-template-default .acf-fc-popup a[data-layout="the_content"]{
            display: none;
        }

    </style>

    <script type="text/javascript">

        (function($) {

            
            $(document).ready(function(){

                <?php 

                global $post;
                
                // Set a javascript variabel "$template_name" based on selected template in WP
                // The variable is then used in window.UpdateACFView to add a CSS class
                // name to the body-tag. Since standard/custom post types doesn't have 
                // page templates we need to set this variable on the first page load
                
                if ($post->post_type == "faq") : // set 'post' for standard post
                    echo 'var $template_name = "post-type/faq";';                   
                else :
                    // Just get the template name
                    echo 'var $template_name = $("#page_template").val();';
                endif; 
                
                ?>

                // Add classes to body 
                window.UpdateACFView = function($template_name) {

                    if ($template_name == "template-home.php") {   
                        $('body').addClass('has-template-home');                            
                    }

                    if ($template_name == "default") {   
                        $('body').addClass('has-template-default');                            
                    }

                    if ($template_name == "template-full-width.php") {   
                        $('body').addClass('has-template-landing');                            
                    }

                }
                
                window.UpdateACFView($template_name); 

            });

            // When a user change the template in the dropdown we need to remove all our custom classes on the body-tag
            // and then, when ajax is completed, trigger window.UpdateACFView in order to set the correct body-class
            $(document).on('change', '#page_template', function(){

                var $template_name = $('#page_template').val();

                $(document).on('ajaxComplete', function(){
                    $('body').removeClass('has-template-home has-template-default has-template-landing'); // Clear all custom classes
                    window.UpdateACFView($template_name); 
                    $(document).off('ajaxComplete');
                });
            });
            
        })(jQuery);  

    </script>  

    <?php
}
add_action('acf/input/admin_head', 'acf_admin_head_layout');





?>