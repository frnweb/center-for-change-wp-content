��          T       �       �      �   T   �   *   �           &  M   @  �  �       T   *  .        �     �  M   �   ACF Tooltip ACF Tooltip requires Advanced Custom Fields 5 (Pro) to be installed &amp; activated. Displays ACF Field description as tooltips Thomas Meyer http://www.dreihochzwo.de http://www.dreihochzwo.de/advanced-custom-fields-show-instructions-as-tooltip Project-Id-Version: ACF Tooltip
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-11-07 12:50+0000
PO-Revision-Date: 2016-11-07 12:50+0000
Last-Translator: thomas <thomas.meyer@dreihochzwo.de>
Language-Team: German
Language: de-DE
Plural-Forms: nplurals=2; plural=n != 1
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco - https://localise.biz/ ACF Tooltip Für ACF Tooltip muss Advanced Custom Fields 5 (Pro) installiert und aktiviert sein. Zeigt die ACF Feld Beschreibung als Tooltip an Thomas Meyer http://www.dreihochzwo.de http://www.dreihochzwo.de/advanced-custom-fields-show-instructions-as-tooltip 