<?php get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

          <?php get_template_part( 'parts/loop', 'pagetitle' ); ?>
	 
			    <?php endwhile; endif; ?>	
   
    <div id="content" class="inner">
		<div id="inner-content" class="row expanded large-collapse medium-collapse">
	
		    <main id="main" class="large-9 medium-9 large-push-3 medium-push-3 columns" role="main">
				
                <section class="entry-content" style="padding-bottom: 0px;"><?php the_content(); ?></section>
                
					<?php
				   
		     $args = array(
				'post_type'      => 'testimonial_post',
				'posts_per_page' => -1,
				'orderby'          => 'rand',
				
			 );

			// The Query
			$testimonial = new WP_Query( $args );
			
			// The Loop
			if ( $testimonial->have_posts() ) {
				echo '<div id="testimonial_holder"><section class="entry-content">';
				while ( $testimonial->have_posts() ) {
					$testimonial->the_post();
					echo '<blockquote class="inverse">&ldquo;' . get_the_content() . '&rdquo;';
					echo '&nbsp;&nbsp;<span class="author">' . get_field('quoted_person') . '</span>';
					echo '</blockquote>';
					echo '<div class="divider clearfix"></div>';
				}
				echo '</section>';
				echo '</div>';
				/* Restore original Post Data */
				wp_reset_postdata();
			} else {
				// no posts found
			}
			?>							
			    					
			</main> <!-- end #main -->
            
            <!-- this will grab the sidebar menu depending on the page -->
           
            
			<div id="sidebar1" class="sidebar large-3 medium-3 large-pull-9 medium-pull-9 columns" role="complementary">

		    <?php get_sidebar(); ?>
            
            </div><!-- end sidebar 1 -->
		    
		</div> <!-- end #inner-content -->
	</div><!-- end #content -->


<?php get_footer(); ?>