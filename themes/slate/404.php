<?php 

///////
//Prepare the keyword search by pulling words from the URL

//$s = str_replace(get_site_url(),"",$wp_query->query_vars['name']);  //keeping here just in case
$s = str_replace("/","",$_SERVER['REQUEST_URI']);
//$s = trim(preg_replace("/(.*)-html|htm|php|asp|aspx)$/","$1",$s));


	$s = trim(str_replace("-"," ",$s));
	$s = urldecode($s);
	$s = strtolower(preg_replace('/[0-9]+/', '', $s )); //remove all numbers
	$stop_words = array(
		"for",
		"the",
		"and",
		"an",
		"a",
		"is",
		"are",
		"than",
		"that",
		"I",
		"to",
		"on",
		"it",
		"with",
		"can",
		"be",
		"of",
		"get",
		"in",
		"you",
		"from",
		"if",
		"by",
		"so",
		"at",
		"do",
		"&",
		"there",
		"too"
	);
	$i=1;
	foreach($stop_words as $word){
		/*
		//for testing:
		if($i==1) {
			echo $s."<br />";
			echo $word."<br />";
			echo "string: ".strlen($s);
			echo "; word: ".strlen(" ".$word)."<br />";
			echo "position: ".strpos($s,$word." ")."<br />";
			echo "string without word: ".(strlen($s)-strlen(" ".$word))."<br />";
		}
		*/
		$word = trim(strtolower($word));
		$s = str_replace(" ".$word." "," ",$s); ///in the middle
		if(strpos($s,$word." ")===0) $s = str_replace($word." ","",$s); // at the beginning
		if(strpos($s," ".$word)==strlen($s)-strlen(" ".$word)) $s = str_replace(" ".$word,"",$s); // at the end
		$i++;
	}
	
	///////
	//Prepare the list of search results

	//future option: checking if only one page returned, then immediately forwarding the person to that page instead
	//check if relevanssi plugin is activated
	if (function_exists('relevanssi_do_query')) {
		$url_terms_search = new WP_Query();
		$url_terms_search->query_vars['s']				=$s;
		$url_terms_search->query_vars['posts_per_page']	=8;
		$url_terms_search->query_vars['paged']			=0;
		$url_terms_search->query_vars['post_status']	='publish';
		relevanssi_do_query($url_terms_search);
     }
     else {
	    //global $wpdb;
		$url_terms_search = new WP_Query( array( 
			's' => 'treatment', 
			//'page_id' => 26,
			'post_type' => 'any', //array( 'post', 'page' ),
			'posts_per_page' => 8,
			'post_status' => 'publish'
		));
     }




get_header(); ?>
	<header class="article-header">
		<h1 class="page-title">Page Not Found</h1>
			</header>
	<div id="content" class="inner page--404">
	
	
		<div id="inner-content" class="row expanded large-collapse medium-collapse">
	
			<main id="main" class="large-12 medium-12 columns" role="main">

				<article id="content-not-found">
				
					<section class="entry-content">
					<div class="callout">
					<?php
	                if ( isset($url_terms_search) ) {
	                if ( $url_terms_search->have_posts() ) { ?>

			            <h1><?php _e( 'The article you were looking for was not found, but maybe you were looking for one of these:', 'jointswp' ); ?></h1>
							
							<ul class="frn_suggestions">
							<?php while ( $url_terms_search->have_posts() ) {
								$url_terms_search->the_post(); ?>
								<li>
									<a href="<?php the_permalink();?>"><?php the_title();?></a>
								</li>
							<?php }	?>
							</ul>
						
						
							
					<?php
					} 
					else 
					{ //if no results returned...
					?>
						<p>
							<?php _e( 'The article you were looking for was not found and we can\'t find any posts that relate to that address.', 'jointswp' ); ?>
						</p>
						<p>
							Take a look at it in the address bar above and see if it looks pretty normal. 
							Make corrections if not and try again or use our search below to check things out.
						</p>
					<?php 
					} //end check if search returns results
					} //ends check if url_terms_search var even set
					?>
					
					<div class="divider"></div>
					<!-- /.divider -->
					<section class="search">
				   <h5>Or Try Searching The Site...</h5>
					   <?php get_search_form(); ?>
					</section> <!-- end search section -->
					</div>
							<!-- /.callout -->
					</section> <!-- end article section -->

			
				</article> <!-- end article -->
	
			</main> <!-- end #main -->

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>