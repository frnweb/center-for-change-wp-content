<!doctype html>
<html class="no-js"  <?php language_attributes(); ?>>
	<head>
		<meta charset="utf-8">
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">
		<link href="<?php echo get_template_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/assets/images/win8-tile-icon.png">
		<meta name="theme-color" content="#121212">
		<?php } ?><!-- this ends the conditional statement for the favicon -->
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/rolling-hills-hospital.jpg" />
		<meta property="og:image:width" content="1200" />
		<meta property="og:image:height" content="717" />
		<?php wp_head(); ?>
		
		<!-- Drop Google Analytics here -->
		<!-- end analytics -->
	</head>
	
	
	<?php

	//Start defining variables for building the header
	$sticky = get_field('sticky_option', 'option');
	$navtype = get_field('nav_type', 'option');


	if ($sticky) {
		$stickycontainer = '<div class="stickynav--container" data-sticky-container>';
		$stickynavigation = ' data-sticky data-options="marginTop:0;" style="width:100%;"';
	}


	?>

	<body <?php body_class(); ?>>
		<div class="off-canvas-wrapper">
			
			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
				
				<?php get_template_part( 'parts/content', 'offcanvas' );
				
				echo '<div class="off-canvas-content" data-off-canvas-content>';
					
					// START HEADER
					echo $stickycontainer;
					echo '<header id="globalheader" class="header" role="banner"'.$stickynavigation.'>';

					?>
						<!-- Nav-Type Default(Mega Menu) -->
						<?php if ($navtype == 'default') { ?>
							<!-- Mega-Menu Nav with Offcanvas-topbar -->
							<div id="top-section">
								<div class="inner">
									<div id="mobilephone" class="show-for-small-only">Confidential and Private  <strong><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sticky Header bar on Mobile"]'); ?></strong></div><!-- end mobile phone -->
									
									<div class="row expanded collapse">
										<div id="leftcontent" class="large-4 medium-3 small-9 columns">
											
											<a href="<?php echo home_url('/'); ?>">
											
											<!-- this grabs the Logo from the themes page, if they do not upload one it defaults to the one below -->
											<?php if( get_field('site_logo', 'option') ): ?>
											  <img src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />
												
											<?php else :?>
											  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/generic_logo.svg" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />	

											<?php endif; ?>
											
											</a>
										</div><!-- /#left content -->
										
										<!--start the mobile menu button -->
										<div id="mobilemenu" class="show-for-small-only small-3 columns">
											<ul class="menu">
												<li>
													<button type="button" class="hamburger nav-button hamburger--squeeze" data-toggle="off-canvas" onClick="ga('send', 'event', 'hamburger Menu', 'opens mobile menu button');">
														<span class="hamburger-box">
															<span class="hamburger-inner"></span>
														</span>
													</button>
												</li>
												<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
												</ul><!-- end ul.menu -->
										</div><!-- end #mobilemenu -->
												
												
										<div id="rightcontent" class="large-8 medium-9 columns hide-for-small-only">
													
													<div class="row expanded large-collapse medium-collapse">
														<div id="usernav" class="large-8 medium-8 columns">
															<nav id="user-based" class="float-right">
																
																<?php wp_nav_menu( array( 'theme_location' => 'user-links',
																	'depth' => 1) ); ?>
																
															</nav><!-- end user-based -->
														</div><!-- /.large-7 -->
																
																<div id="callinfo" class="large-4 medium-4 columns">
																	<div id="phone">
																	<h1><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Desktop Header"]'); ?></h1>
																	</div><!-- end #phone -->
																</div><!-- #call info -->
													</div><!-- /.row -->
																	
																	
																	
												</div><!-- /#right content -->
										</div> <!-- /.row expanded -->
									</div>  <!-- /.inner -->
																	
							</div><!-- end #top-section -->

							<!-- This navs will be applied to the topbar, above all content
							To see additional nav styles, visit the /parts directory -->
							<?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>

						<?php } ?>
						<!-- END Default(Mega Menu) -->

						<!-- Nav-Type Title-Bar -->
						<?php if ($navtype == 'title-bar') {
							get_template_part( 'parts/nav', 'title-bar');
						} ?>
						<!-- END Title-Bar -->
						

						
															
					</header> <!-- end .header -->
					<?php 
						if ($sticky) {
							echo '</div>';
						}
					?>











