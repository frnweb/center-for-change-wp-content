<a id="backtop" class="button" href="#globalheader" onClick="ga('send', 'event', 'back to top', 'back to top');"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

		<?php $site_title = get_bloginfo( 'name' ); //defining the name of the site according to wordpress?>
			
			
				<?php /*?><?php get_template_part( 'parts/footer', 'nav' ); ?><?php */?>
                   <!-- need to build an option here that allows them to add a beacon module to the Theme options then it will activate this Template part -->
                    

                 <!-- this grabs the phone number in the area above the footer //possibly replace inside of part with a Bilboard module that is in the footer options of theme-->
                  <?php /*?><?php get_template_part( 'parts/footer', 'callnow' ); ?><?php */?>
                   
                 <!-- this grabs the map and location information for the facility- need to replace contents with a module in the Footer theme options section-->
                  <?php get_template_part( 'parts/footer', 'location' ); ?>   
                    
                    <footer class="footer" role="contentinfo">
                    
	                    <div class="inner section">
		                    <div class="row footer-branding expanded large-collapse medium-collapse">
		                    	<div class="columns large-8 medium-7">
			                    	<a href="<?php echo home_url('/'); ?>">
			                    	<?php if( get_field('footer_logo', 'option') ): ?>
													  <img src="<?php the_field('footer_logo', 'option'); ?>" alt="<?php echo $site_title ?>" class="img-responsive" id="logo" />
														
													<?php else :?>
														<img src="<?php echo get_template_directory_uri(); ?>/assets/images/generic_logo.svg" alt="<?php echo $site_title ?>" class="img-responsive" id="logo" />	
										<?php endif; ?>
			                    	</a><!-- end footer logo -->
		                    	</div><!-- /.columns large-6 -->
		                    	
		                       	<div class="columns large-4 medium-5">
		                        	<?php get_template_part( 'parts/footer', 'socials' ); ?>
		                        </div><!-- /.columns large-4 -->

		                    </div> <!-- /.row -->
	                    
	                    	<nav role="navigation" class="show-for-medium clearfix">
			    				<?php joints_footer_links(); ?>
			    			</nav><!-- /navigation -->

	                    </div><!-- /.inner section -->
                            	
                    <?php echo do_shortcode('[frn_footer]'); ?>
 
					</footer> <!-- end .footer -->
			
            	</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
        
		<?php wp_footer(); ?>
    
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCadueHkRtxJPchYRSuemPn-uyLKCDh9IA"></script>


	</body>
</html> <!-- end page -->