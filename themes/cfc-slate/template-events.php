<?php
/*
Template Name: Events Page 
*/
?>

<?php get_header(); ?>
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>	

			 <article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/WebPage">
  	 <section itemprop="articleBody">

		 <header class="article-header <?php wpb_bg();?>">
		 </header>
			
	<div id="content" class="inner page--events">
	
	<div class="inner">
	<div class="entry-content">

		<?php the_content(); ?>			
	
	</div><!-- end entry-content -->
	</div><!-- end inner -->

	</div> <!-- end #content -->
				 </section></article>
				 
				 <?php endwhile; endif; ?>

<?php get_footer(); ?>