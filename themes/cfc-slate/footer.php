<a id="backtop" class="button" href="#top_anchor" onClick="ga('send', 'event', 'back to top', 'back to top');"><i class="fa fa-angle-up" aria-hidden="true"></i></a>

		<?php $site_title = get_bloginfo( 'name' ); //defining the name of the site according to wordpress?>
				
				
				<div class="footer__modules">
				<?php get_template_part( 'modules/custom', 'module' ); ?>
                   
                 <!-- this grabs the phone number in the area above the footer //possibly replace inside of part with a Bilboard module that is in the footer options of theme-->
                  <?php get_template_part( 'parts/footer', 'callnow' ); ?>
                   
                 <!-- this grabs the map and location information for the facility- need to replace contents with a module in the Footer theme options section-->
                  <?php get_template_part( 'parts/footer', 'location' ); ?>   
                   
                   
                    <?php get_template_part( 'parts/footer', 'tricare' ); ?>
				</div><!-- end class .footer___modules --->
                    
                    <footer class="footer" role="contentinfo">
                    
	                    <div class="inner section">
		                    <div class="row footer-branding expanded large-collapse medium-collapse">
		                    	<div class="columns large-5 medium-5 large-push-7 medium-push-7">
			                  
			                 <!-- BEGIN: Constant Contact Email List Form Button -->
			                 <div class="newsletter"><a href="http://visitor.r20.constantcontact.com/d.jsp?llr=6jwt7qjab&amp;p=oi&amp;m=1109742180669&amp;sit=8lhzjw9gb&amp;f=d5b2c506-34bf-4345-8de2-878f936bdc6f" class="button" target="_blank">Join Our Newsletter</a><!-- BEGIN: Email Marketing you can trust -->
<?php /*?>			                 <div id="ctct_button_footer" style="font-family:Arial,Helvetica,sans-serif;font-size:10px;color:#999999;margin-top: 10px;" align="center"></div>
<?php */?>			                 </div><!-- end newsletter -->
			                  
		                    	</div><!-- /.columns large-6 -->
		                    	
		                       	<div class="columns large-7 medium-7 large-pull-5 medium-pull-5">
		                        	<?php get_template_part( 'parts/footer', 'socials' ); ?>
		                        </div><!-- /.columns large-4 -->

		                    </div> <!-- /.row -->
	                    
	                    	<nav role="navigation" class="show-for-medium clearfix">
			    				<?php joints_footer_links(); ?>
			    			</nav><!-- /navigation -->

	                    </div><!-- /.inner section -->
					<p class="tricare-text">
					<a href="https://centerforchange.com/standard-services/">Standard Services</a> | <a href="https://centerforchange.com/wp-content/uploads/Language-Assistance.pdf">Language Assistance</a> | <a href="https://centerforchange.com/nondiscrimination-notice/">Nodiscrimination Notice</a>
						</p>
						<p class="tricare-text">TRICARE<sup>&reg;</sup> is a registered trademark of the Department of Defense, Defense Health Agency. All rights reserved. Physicians are on the medical staff of Center for Change, but, with limited exceptions, are independent practitioners who are not employees or agents of Center for Change. The facility shall not be liable for actions or treatments provided by physicians. Model representations of real patients are shown. Actual patients cannot be divulged due to HIPAA regulations.</p>
                    <?php echo do_shortcode('[frn_footer]'); ?>
 
					</footer> <!-- end .footer -->
			
            	</div>  <!-- end .main-content -->
			</div> <!-- end .off-canvas-wrapper-inner -->
		</div> <!-- end .off-canvas-wrapper -->
        
		<?php wp_footer(); ?>
    
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCadueHkRtxJPchYRSuemPn-uyLKCDh9IA"></script>


	</body>
</html> <!-- end page -->