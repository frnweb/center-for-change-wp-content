<?php
/*
Module: Custom
*/
?>


<div class="module module--custom" >
	 <div class="inner expanded" data-equalizer data-equalize-on="medium">
			<div class="row expanded collapse">
				<div class="columns large-8 medium-7 small-12">
				
				<div class="slider__container">
				
					<?php

					// The Query
					$quotes = new WP_Query( array( 'post_type' => 'testimonial_post' ) );

					// The Loop
					if ( $quotes->have_posts() ) {
						echo '<ul id="module--custom">';
						while ( $quotes->have_posts() ) {
							$quotes->the_post();
							echo '<li><blockquote data-equalizer-watch>&ldquo;' . get_the_content() . '&rdquo;</blockquote></li>';
						}
						echo '</ul>';
						/* Restore original Post Data */
						wp_reset_postdata();
					} else {
						// no posts found
						
					}
					?>
					</div><!-- end <div class="slider__container">-->
	
				</div>
				<!-- /.columns -->
				<div class="columns large-4 medium-5 small-12">
					<div class="cta-box" data-equalizer-watch>
					<h2>Do I have an eating disorder?</h2>
					<a href="<?php echo home_url('/'); ?>admissions/do-i-have-an-eating-disorder/" class="button">Take our assessment</a>
					</div><!-- end cta box-->
				</div>
				<!-- /.columns -->
			</div>
			<!-- /.row expanded collapse -->	
	</div><!-- end inner -->
</div><!-- end module -->

 