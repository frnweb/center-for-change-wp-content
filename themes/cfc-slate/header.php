<!doctype html>
<html class="no-js"  <?php language_attributes(); ?>>
	<head>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WRPBK2K');</script>
<!-- End Google Tag Manager -->

		<meta charset="utf-8">
		<!-- Force IE to use the latest rendering engine available -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta class="foundation-mq">
		
		<!-- If Site Icon isn't set in customizer -->
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) { ?>
		<!-- Icons & Favicons -->
		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png">
		<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/apple-icon-touch.png" rel="apple-touch-icon" />
		<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<meta name="theme-color" content="#121212">
		<?php } ?><!-- this ends the conditional statement for the favicon -->
		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
		<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/center-for-change.jpg" />
		<meta property="og:image:width" content="1200" />
		<meta property="og:image:height" content="717" />
		<?php wp_head(); ?>
		
		<!--[if lte IE 10]>
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/ie.css" />
		<![endif]-->
		
	</head>
	
	<?php

	//Start defining variables for building the header
	$sticky = get_field('sticky_option', 'option');
	if ($sticky) {
		$stickycontainer = '<div class="stickynav--container" data-sticky-container>';
		$stickynavigation = ' data-sticky data-options="marginTop:0;" style="width:100%;"';
	}


	?>
	
	<body <?php body_class(); ?>>
		<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WRPBK2K"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

		<div class="off-canvas-wrapper">
			
			<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
				
				<?php get_template_part( 'parts/content', 'offcanvas' );
				
				echo '<div class="off-canvas-content" data-off-canvas-content>';
				
				echo '<div id ="top_anchor" style="visibility: hidden;"></div>';
					
					// START HEADER
					echo $stickycontainer;
					echo '<header id="globalheader" class="header" role="banner"'.$stickynavigation.'>';

					?>
				
				
						<div id="top-section">
							<div class="inner">
								<div id="phonemobile" class="show-for-small-only">Confidential and Private  <?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Sticky Header bar on Mobile"]'); ?></div><!-- end mobile phone -->
								
								<div class="row expanded collapse">
									<div id="leftcontent" class="large-3 medium-3 small-9 columns">
										
										<a href="<?php echo home_url('/'); ?>">
										
										<!-- this grabs the Logo from the themes page, if they do not upload one it defaults to the one below -->
										<?php if( get_field('site_logo', 'option') ): ?>
										  <img src="<?php the_field('site_logo', 'option'); ?>" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />
										
										<?php else :?>
										  <img src="<?php echo get_template_directory_uri(); ?>/assets/images/generic_logo.svg" alt="<?php bloginfo('name'); ?>" class="img-responsive" id="logo" />	

										<?php endif; ?>
										
										</a>
									</div><!-- /#left content -->
									
									<!--start the mobile menu button -->
									<div id="mobilemenu" class="show-for-small-only small-3 columns">
										<ul class="menu">
											<li>
											<button type="button" class="hamburger nav-button hamburger--squeeze" data-toggle="off-canvas" onClick="ga('send', 'event', 'hamburger Menu', 'opens mobile menu button');">
													<span class="hamburger-box">
														<span class="hamburger-inner"></span>
													</span>
												</button>
											</li>
											<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
											</ul><!-- end ul.menu -->
									</div><!-- end #mobilemenu -->
											
											
									<div id="rightcontent" class="large-9 medium-9 columns hide-for-small-only">
												
												<div class="row expanded large-collapse medium-collapse">
													<div id="usernav" class="large-10 medium-9 columns">
													
												
															
														<nav id="user-based" class="float-right">
															
															<?php wp_nav_menu( array( 'theme_location' => 'user-links',
																'depth' => 1) ); ?>
															
														</nav><!-- end user-based -->
														
															 <div class="button--search--header show-for-medium float-right">
         <a class="search button" data-toggle="header__searchform" onClick="ga('send', 'event', 'search button', 'opens search bar');"></a>
															</div><!-- end the search button in the top header -->
														
																
														 
													</div><!-- /.large-7 -->
												
															
															<div id="callinfo" class="large-2 medium-3 columns">
																<div id="phone">
																<?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Desktop Header"]'); ?>
																</div><!-- end #phone -->
															</div><!-- #call info -->
												</div><!-- /.row -->
																
												<!-- This navs will be applied to the topbar, above all content
																To see additional nav styles, visit the /parts directory -->
																<?php get_template_part( 'parts/nav', 'offcanvas-topbar' ); ?>
																
																										
																
											</div><!-- /#right content -->
									</div> <!-- /.row expanded -->
								</div>  <!-- /.inner -->
																
							</div><!-- end #top-section -->
															
																
					</header> <!-- end .header -->
					
				    		<div id="header__searchform" data-reveal class="full reveal">
				    		<div class="inner">
				    		 <button class="close-button button--close" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
							  </button>
							  <p>Begin searching and quickly find what you need...</p>
				    		<?php get_search_form(); ?>
								</div><!-- end inner -->
				    		</div><!-- end header search -->
				    		
				    		<?php 
						if ($sticky) {
							echo '</div>';
						}
					?>
				    		
