module.exports = function(grunt) {
  // Configure task(s)
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    
    // uglify
    uglify: {
      dev: {
        options: {
          mangle: true,
          compress: true,
        },
        src: [
          'js/child.js',
          'js/custom-bxsliders.js',
          '!js/*.min.js'
        ],
        dest: 'js/child.min.js'
      },
    },// /uglify
    

    // sass
    sass: {
      dev: {
        options: {
            sourceMap: true
        },
        files: {
                'css/child.css':'scss/main.scss'
        }
      }
    }, // /sass


    // cssmin
    cssmin: {
      options: {
        restructuring: false,
        shorthandCompacting: false,
        roundingPrecision: -1,
        sourceMap: true
      },

      dev: {
        files: {
          'css/child.min.css': ['css/*.css', '!css/ie.css', '!css/*.min.css']
        }
      }
    },// /cssmin

    // tinypng
    tinypng: {
      options: {
        apiKey: "4Ouz0gfzly_SxeykW3oPt1dwhFNKmU7e",
        checkSigs: true,
        sigFile: 'images/file_sigs.json',
        summarize: true,
        stopOnImageError: true
      },
      
      png: {
        expand: true,
        cwd: 'images/originals',
        src: ['**/*.png'],
        dest: 'images/compressed/',
        ext: '.min.png',
        extDot: 'first'
      },
      jpg: {
        expand: true,
        cwd: 'images/originals',
        src: ['**/*.jpg'],
        dest: 'images/compressed/',
        ext: '.min.jpg',
        extDot: 'first'
      }
    },// /tinypng
	  
	//autoprefix
    postcss: {
      options: {
        map: true,
        processors: [
          require('autoprefixer')({grid: 'autoplace'}),
        ]
      },
      dist: {
        files: [
          {
              expand: true,
              src: ['css/child.css', 'css/child.min.css'],
              ext: '.min.css',
              extDot: 'first'
          }
        ]
      }
    },// /autoprefix


    // watch
    watch: {
      js: {
        files: ['js/*.js', '!js/*.min.js'],
        tasks: ['uglify:dev']
      },

      sass: {
        files: ['scss/*.scss', 'scss/modules/*.scss', 'scss/parts/*.scss'],
        tasks: ['sass:dev']
      },
		
	 autoprefix: {
        files: 'css/child.css',
        tasks: ['postcss']
      },	

      css: {
        files: ['css/*.css', '!css/*.min.css'],
        tasks: ['cssmin:dev']
      }
    }, // /watch


    // Browsersync
    browserSync: {
      bsFiles: {
        src: 'css/*.css'
      },
      options: {
            watchTask: true, 
            proxy: 'https://centerforchange.local/'
            
      }
    }// /Browsersync


  });




  // Load the plugins
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-sass');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-tinypng');
  grunt.loadNpmTasks('grunt-postcss');

  // Register task(s).
  grunt.registerTask('default', ['uglify:dev', 'sass:dev','postcss', 'cssmin:dev', 'browserSync', 'watch']);


};