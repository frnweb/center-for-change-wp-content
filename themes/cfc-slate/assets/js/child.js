// Put your custom Javacsrip functions here. Make sure they don't conflict with the Custom.js file in the Parent theme.

//Animates sections when they are reached on scroll- based on viewportchecker.js

$(document).ready(function() {
	"use strict";
	jQuery('.animated-section').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
	    offset: 100    
	   });  
	
	jQuery('.staff--deck .staff__card').addClass("hidden").viewportChecker({
	    classToAdd: 'visible animated fadeInUp', // Class to add to the elements when they are visible
	    offset: 100    
	   });  
	    
}); 

////adds class on hover to the header  
$('#top-bar-menu ul.mega-menu li.has-dropdown-arrow').hover(
       function(){ $('#globalheader').addClass('mega-is-open') },
       function(){ $('#globalheader').removeClass('mega-is-open') }
);


////equalizes some of the modules that have to be initialized after the load 
// Duo Gallery
$(document).ready(function(){
  "use strict";
	
///DUO equalizers 
  $('.duo--image .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--image .row').attr("data-equalize-on", "medium");
  
  
  
  $('.duo--video .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--video .row').attr("data-equalize-on", "medium");
  $('.duo--video .embed-responsive-item').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--video .flex-video').attr("data-equalizer-watch", "duo__equalizer");
 
  
  $('.duo--gallery .row').attr("data-equalizer", "duo__equalizer");
  $('.duo--gallery .row').attr("data-equalize-on", "medium");
  $('.duo--gallery .bx-viewport').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--gallery li').attr("data-equalizer-watch", "duo__equalizer");
  $('.duo--gallery li').foundation();


  $('.duo .row').foundation();

///Carousel equalizers 
  $('.carousel--custom .bx-viewport').attr("data-equalizer", "carousel--custom");
  $('.carousel--custom .bx-viewport').attr("data-equalize-on", "medium");
  $('.carousel--custom .bx-viewport').foundation();
	//carousel controls
	$('.carousel--pageitems .bx-viewport').attr("data-equalizer", "column");
    $('.carousel--pageitems .bx-viewport').attr("data-equalize-on", "medium");
    $('.carousel--pageitems .bx-viewport').foundation();
    $('.button--pageitems').css({
        "position": "absolute",
        "bottom": "0px",
        "left": "50%",
        "transform": "translateX(-50%)"
    });
//	
	
	
});
