<?php
function child_theme_enqueue_styles() {

    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array(), '', 'all'  );
	wp_enqueue_style( 'motion-ui-css', get_template_directory_uri() . '/vendor/motion-ui/dist/motion-ui.min.css', array(), '', 'all' );
	wp_enqueue_style( 'foundation-css', get_template_directory_uri() . '/vendor/foundation-sites/dist/foundation.min.css', array(), '', 'all' );
	
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array(), '', 'all'  );//this replaces parent theme style.css file and is required by wordpress

	wp_enqueue_style( 'slate-css', get_template_directory_uri() . '/assets/css/slate.min.css', array(), '', 'all'  );//eventually we will swap this out with minified version
	
	wp_enqueue_style( 'child-css', get_stylesheet_directory_uri() . '/assets/css/child.min.css', array(), '', 'all'  );//this replaces parent theme style.css file and is required by wordpress
	
	// Enqueue Google Fonts here
    wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,600,600i,700,700i', false );

	//JS
    wp_enqueue_script( 'child-js', get_stylesheet_directory_uri() . '/assets/js/child.min.js', array( 'jQuery', 'foundation-js', 'slate-js' ), '', true );

    // Enqueue Adobe Typekit here
    wp_enqueue_script( 'theme_typekit', '//use.typekit.net/gdc7naj.js');


	
	///If you add new documents to the child theme, you have to make sure none of these names overwrite the parent files, unless that is the intention.


}
add_action( 'wp_enqueue_scripts', 'child_theme_enqueue_styles' );

/////special function for page headers 
require_once(get_stylesheet_directory().'/assets/functions/headings.php');




// Checks if typekit script exists (theme_typekit), adds additional script to header.
function theme_typekit_inline() {
  if ( wp_script_is( 'theme_typekit', 'done' ) ) { ?>
  	<script>try{Typekit.load({ async: false });}catch(e){}</script>
<?php }
}
add_action( 'wp_head', 'theme_typekit_inline' );


//function exclude_category( $query ) {
//	///this hides the news category on the Resources page
//    if ( $query->is_home() && $query->is_main_query() ) {
//        $query->set( 'cat', '-3' );
//    }
//}
//add_action( 'pre_get_posts', 'exclude_category' );


///this is extending a shortcode that they had on their old site through the Geneis showroom theme 
/* infoboxes */
function zp_infoboxes( $atts, $content = null ) {
	extract( shortcode_atts( array(
		"style" => ''
	), $atts));
	
	return '<div class="'.$style.' callout">'.$content.'</div>';
	
}
add_shortcode( 'infobox', 'zp_infoboxes');
