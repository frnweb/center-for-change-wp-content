<?php get_header(); ?>

    
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'pagetitle' ); ?>
	<div id="content" class="inner page--resources">
			<div id="inner-content" class="row expanded large-collapse medium-collapse">	
			
					    
					<?php $sidebarlayout = get_field('resources_sidebar_layout', 'option');
					$largesidebarwidth = get_field('large_resources_sidebar_width', 'option');
					$mediumsidebarwidth = get_field('medium_resources_sidebar_width', 'option');

					$largewidth = (12 - $largesidebarwidth);
					$mediumwidth = (12 - $mediumsidebarwidth);

					// sidebar
					if($sidebarlayout == 'left') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' columns" role="complementary">';
					}

					if($sidebarlayout == 'right') {
					echo '<div id="sidebar1" class="sidebar large-'.$largesidebarwidth.' medium-'.$mediumsidebarwidth.' large-push-'.$largewidth.' medium-push-'.$mediumwidth.' columns" role="complementary">';
					}
					echo get_sidebar();
					echo '</div>';//end the sidebar

					// #main
					if($sidebarlayout == 'left') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' columns has-sidebar" role="main">';
					}
					if($sidebarlayout == 'right') {
					echo '<main id="main" class="large-'.$largewidth.' medium-'.$mediumwidth.' large-pull-'.$largesidebarwidth.' medium-pull-'.$mediumsidebarwidth.' columns has-sidebar" role="main">';
					}	
				?>
			
			
	<?php

	// The Query
	$article_loop = new WP_Query( 
		array( 'category_name' => 'professional-article' ) 
	);

	// The Loop
	if ( $article_loop->have_posts() ) {
		echo '';
		while ( $article_loop->have_posts() ) {
			$article_loop->the_post();
			
			echo '<div class="row expanded">';
			echo '<div class="post_holder clearfix">';
			echo '<article id="post-'.get_the_ID().'"';
			echo post_class('');
			echo 'role="article" itemscope itemtype="http://schema.org/BlogPosting">';
			if (has_post_thumbnail()) {
			echo '<div class="columns featured_image large-5 medium-12">';
                    echo '<a href="'.get_the_permalink().'">';
					echo the_post_thumbnail('full');
					echo '</a>';
					echo '</div>';///end column for featured image 
			echo '<div class="post_excerpt columns large-7 medium-12">';
			}//end if statement
			else {
				echo '<div class="post_excerpt columns large-12 medium-12">';
			}///end conditional statement 
			echo '<header class="post-header">';
				echo '<h2><a href="'.get_the_permalink().'" rel="bookmark">';//need to figure out how to add the title attribute
				echo the_title();
				echo '</a></h2>';
				echo '</header>';
    			echo '<section class="post-excerpt" itemprop="articleBody">';
				echo the_excerpt('<button class="tiny">' . __( 'Read more...', 'jointswp' ) . '</button>'); 
				echo '</section>';
			echo '</div>';///end column for the excerpt
			echo '</article>';//end #post
			echo '</div>';///end post holder 
			echo '</div>';///end row
		}
		echo '';
		/* Restore original Post Data */
		wp_reset_postdata();
	} else {
		// no posts found
	}?>
	
		<?php joints_page_navi(); ?>
		
		</main><!-- end #main -->
		</div><!-- end inner content -->
	</div><!-- end #content -->

<?php get_footer(); ?>