    
     <div id="callnow">  
     <div class="inner">
		 <h2><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/phone-icon.svg" width="25"/>Call Us Today <strong class="phonenumber"><?php echo do_shortcode('[frn_phone ga_phone_location="Phone Clicks in Footer"]'); ?></strong> </h2>
               <div class="divider"></div>
                <?php /*?><p>Your Call is Confidential & Private.</p><?php */?>
                
            <div class="callnow__logo">
            	<img src="<?php the_field('site_logo', 'option'); ?>" alt="<?php echo $site_title ?>" />  
            </div><!-- end call now logo -->
  		</div> <!-- /.inner -->
</div><!-- end Call now -->