<div class="tricare">
	<h2><a href="<?php echo home_url('/admissions/working-with-tricare/'); ?>">Tricare<sup>&reg;</sup> Certified</a></h2>
</div><!-- end tricare -->

<div class="logo-section">
	<div class="inner clearfix">
		
			<div class="column column-block"><a href="https://www.bbb.org/utah/business-reviews/psychologists/center-for-change-incorporated-in-orem-ut-6001497" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/bbb.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="http://www.eatingdisorderscoalition.org/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/edc.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="https://natsap.org/Public/Default.aspx?hkey=3eb162c8-8572-48d6-a4b0-8f77303d1751&WebsiteKey=a6db6176-2e1f-4120-ad6e-c64e14a4337a" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/natsap.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="https://www.grammy.org/musicares" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/music-cares.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="https://www.qualitycheck.org/quality-report/?bsnId=172228" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/joint-commisions.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="http://iaedp.com/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/iaedp.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="https://www.eatingdisorderhope.com/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/eating-disorder-hope.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="http://www.advanc-ed.org/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/advanced.min.jpg" /></a></div>
		  
		  <div class="column column-block"><a href="https://www.nationaleatingdisorders.org/" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/compressed/neda.min.jpg" /></a></div>
		  
		  
		
	</div><!-- end inner -->
</div><!-- end logo-section -->