# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Slate Child Theme ###

* This is the child theme for SLATE. SLATE is a modular wordpress theme that relies on the Zurb Foundations 6 framework as well as utilizes Advanced Custom Fields to  

### How do I get set up? ###

* Install this theme in the wp-content/themes root folder and then in wordpress Activate it

### How does it work? ###

* This Child theme is dependent on SLATE for the advanced custom fields, baseline styles, Javascript functions and much more. 

* What this Theme is intended for is to be skinned in it's own custom way. In the "assets/css" root you will find a custom.css that directly mimics the organization of SLATE. This is where you will make your custom changes. 

* Additionally there is a "assets/images" and "assets/js/child.js" where you can place custom images, as well as custom javascript modifications. 

### How do I overwrite files from SLATE? ###

* With this child theme, it is simple to alter the parent file. Simply create a file in the same location as it in SLATE with the same name and now that will override the Parent file. Two instances where this may be done is in the "parts" and "modules" folders where you could make altercations to specific modules or template parts IE the header.php file. 



